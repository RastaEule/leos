package rte;

import java.lang.*;

import common.Bluescreen;
import common.StaticV24;
import kernel.memory.*;

public class DynamicRuntime
{
	public static boolean _debugOutput = false;

	/** Sometimes a new object needs to be bigger than anticipated. This variable contains how many bytes to add to the scalarSize of a new Object
	 * and is controlled by MemoryManagement*/
	public static int scalarSizeDelta = 0;

	public static int nextFreeAddress = -1;
	private static Object _lastHeapObject;

	public static void init()
	{
		nextFreeAddress = MemoryManagement.sjcImageUpperAdr;
	}

	public static Object newInstance(int scalarSize, int relocEntries, SClassDesc type)
	{
		int startAdr = -1;
		// 4 bytes per reloc
		int relocSize = relocEntries * 4;
		// Align to multiple of 4 byte
		scalarSize = (scalarSize + 3) & ~3;
		int totalSize = relocSize + scalarSize;

		// Get Address
		if (MemoryManagement._initializing) {
			startAdr = nextFreeAddress;
			nextFreeAddress += totalSize;
		}
		else {
			scalarSizeDelta = 0;
			startAdr = MemoryManagement.getObjectAddress(totalSize);
			scalarSize += scalarSizeDelta;
			totalSize += scalarSizeDelta;
		}

		int objAdr = startAdr + relocSize;
		// Zero-initialize the allocated memory
		for (int i = startAdr; i < startAdr + totalSize; i += 4)
			MAGIC.wMem32(i, 0);

		// Create new Object
		Object newObj = MAGIC.cast2Obj(objAdr);
		MAGIC.assign(newObj._r_scalarSize, scalarSize);
		MAGIC.assign(newObj._r_relocEntries, relocEntries);
		MAGIC.assign(newObj._r_type, type);

		// Update lastObject
		if (MemoryManagement._initializing){
			if (_lastHeapObject != null)
				MAGIC.assign(_lastHeapObject._r_next, newObj);
			_lastHeapObject = newObj;
		}
		else{
			Object lastObj = MemoryManagement.getLastObject();
			if (lastObj != null)
				MAGIC.assign(lastObj._r_next, newObj);
		}
		MemoryManagement.updateLastObject(newObj);

		if (_debugOutput){
			StaticV24.print("Created Object ");
			StaticV24.print(newObj._r_type.name);
			StaticV24.print(" @");
			StaticV24.printHex(objAdr, 8);
			StaticV24.print(" with Type ");
			StaticV24.print(MAGIC.rMem32(objAdr - 4));
			StaticV24.println();
		}

		return newObj;
	}

	public static Object newEmptyObject(int startAddress, int size)
	{
		SClassDesc type = MAGIC.clssDesc("EmptyObject");
		int scalarSize = MAGIC.getInstScalarSize("EmptyObject"); // Scalar size in bytes. This contols the size of my EmptyObject
		int relocEntries = MAGIC.getInstRelocEntries("EmptyObject"); // RelocEntries count

		int relocSize = relocEntries * 4; // 4 bytes per reloc

		scalarSize = size - relocSize; // Change object size
		scalarSize = (scalarSize + 3) & ~3; // Align to multiple of 4 byte

		// calculate object address inside allocated memory
		int objAdr = startAddress + relocSize;

		// Create new Object
		Object object = MAGIC.cast2Obj(objAdr);
		MAGIC.assign(object._r_scalarSize, scalarSize);
		MAGIC.assign(object._r_relocEntries, relocEntries);
		MAGIC.assign(object._r_type, type);

		if (_debugOutput) {
			StaticV24.print("Created EmptyObject ");
			StaticV24.print(" @");
			StaticV24.printHex(objAdr, 8);
			StaticV24.print(" with Type:");
			StaticV24.print(MAGIC.rMem32(objAdr - 4));
			StaticV24.print(" and size:");
			StaticV24.print(size);
			StaticV24.println();
		}

		return object;
	}

	/** Assign special memory that cannot be deleted later.
	 *  The resulting startAddress is aligned.
	 * **/
	public static int assignSpecialMemory(int size, int alignment)
	{
		int adr = nextFreeAddress;
		if (alignment > 0){
			while(adr % alignment != 0){
				adr++;
			}
		}
		nextFreeAddress = adr + size;

		StaticV24.print("Assigning special memory: 0x");
		StaticV24.printHex(adr, 8);
		StaticV24.print(" - 0x");
		StaticV24.printHex(adr + size, 8);
		StaticV24.println();
		StaticV24.print("Next Free Address: 0x");
		StaticV24.printHex(nextFreeAddress, 8);
		StaticV24.println();

		return adr;
	}

	public static SArray newArray(int length, int arrDim, int entrySize, int stdType, SClassDesc unitType)
	{ //unitType is not for sure of type SClassDesc
		int scS, rlE;
		SArray me;

		if (stdType == 0 && unitType._r_type != MAGIC.clssDesc("SClassDesc")) MAGIC.inline(0xCC); //check type of unitType, we don't support interface arrays
		scS = MAGIC.getInstScalarSize("SArray");
		rlE = MAGIC.getInstRelocEntries("SArray");
		if (arrDim > 1 || entrySize < 0) rlE += length;
		else scS += length * entrySize;
		me = (SArray) newInstance(scS, rlE, MAGIC.clssDesc("SArray"));
		MAGIC.assign(me.length, length);
		MAGIC.assign(me._r_dim, arrDim);
		MAGIC.assign(me._r_stdType, stdType);
		MAGIC.assign(me._r_unitType, unitType);
		return me;
	}

	public static void newMultArray(SArray[] parent, int curLevel, int destLevel, int length, int arrDim, int entrySize, int stdType, SClassDesc clssType)
	{
		int i;

		if (curLevel + 1 < destLevel)
		{ //step down one level
			curLevel++;
			for (i = 0; i < parent.length; i++)
			{
				newMultArray((SArray[]) ((Object) parent[i]), curLevel, destLevel, length, arrDim, entrySize, stdType, clssType);
			}
		}
		else
		{ //create the new entries
			destLevel = arrDim - curLevel;
			for (i = 0; i < parent.length; i++)
			{
				parent[i] = newArray(length, destLevel, entrySize, stdType, clssType);
			}
		}
	}

	public static boolean isInstance(Object o, SClassDesc dest, boolean asCast)
	{
		SClassDesc check;

		if (o == null)
		{
			if (asCast) return true; //null matches all
			return false; //null is not an instance
		}
		check = o._r_type;
		while (check != null)
		{
			if (check == dest) return true;
			check = check.parent;
		}
		if (asCast) MAGIC.inline(0xCC);
		return false;
	}

	public static SIntfMap isImplementation(Object o, SIntfDesc dest, boolean asCast)
	{
		SIntfMap check;

		if (o == null) return null;
		check = o._r_type.implementations;
		while (check != null)
		{
			if (check.owner == dest) return check;
			check = check.next;
		}
		if (asCast) MAGIC.inline(0xCC);
		return null;
	}

	public static boolean isArray(SArray o, int stdType, SClassDesc clssType, int arrDim, boolean asCast)
	{
		SClassDesc clss;

		//in fact o is of type "Object", _r_type has to be checked below - but this check is faster than "instanceof" and conversion
		if (o == null)
		{
			if (asCast) return true; //null matches all
			return false; //null is not an instance
		}
		if (o._r_type != MAGIC.clssDesc("SArray"))
		{ //will never match independently of arrDim
			if (asCast) MAGIC.inline(0xCC);
			return false;
		}
		if (clssType == MAGIC.clssDesc("SArray"))
		{ //special test for arrays
			if (o._r_unitType == MAGIC.clssDesc("SArray")) arrDim--; //an array of SArrays, make next test to ">=" instead of ">"
			if (o._r_dim > arrDim) return true; //at least one level has to be left to have an object of type SArray
			if (asCast) MAGIC.inline(0xCC);
			return false;
		}
		//no specials, check arrDim and check for standard type
		if (o._r_stdType != stdType || o._r_dim < arrDim)
		{ //check standard types and array dimension
			if (asCast) MAGIC.inline(0xCC);
			return false;
		}
		if (stdType != 0)
		{
			if (o._r_dim == arrDim) return true; //array of standard-type matching
			if (asCast) MAGIC.inline(0xCC);
			return false;
		}
		//array of objects, make deep-check for class type (PicOS does not support interface arrays)
		if (o._r_unitType._r_type != MAGIC.clssDesc("SClassDesc")) MAGIC.inline(0xCC);
		clss = o._r_unitType;
		while (clss != null)
		{
			if (clss == clssType) return true;
			clss = clss.parent;
		}
		if (asCast) MAGIC.inline(0xCC);
		return false;
	}

	public static void checkArrayStore(SArray dest, SArray newEntry)
	{
		if (dest._r_dim > 1) isArray(newEntry, dest._r_stdType, dest._r_unitType, dest._r_dim - 1, true);
		else if (dest._r_unitType == null) MAGIC.inline(0xCC);
		else isInstance(newEntry, dest._r_unitType, true);
	}

	public static void nullException() {
		StaticV24.print("Null Exception");
		MAGIC.wMem32(0xB8000, -1);
	}
}