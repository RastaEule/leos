package java.lang;
import rte.*;
import java.lang.*;

public class String
{
	private char[] value;
	private int count;

	public String(char[] data)
	{
		count = data.length;
		value = data;
	}

	public boolean equals(String s)
	{
		if (count == s.count) {
			for(int i = 0; i < count; i++) {
				if (value[i] != s.charAt(i))
					return false;
			}
			return true;
		}
		return false;
	}

	public String add(String s)
	{
		int newLen = length() + s.length();
		char[] newData = new char[newLen];
		for (int i = 0; i < length(); i++) {
			newData[i] = charAt(i);
		}
		for (int i = 0; i < s.length(); i++) {
			newData[length() + i] = s.charAt(i);
		}
		return new String(newData);
	}

	@SJC.Inline
	public int length()
	{
		return count;
	}

	@SJC.Inline
	public char charAt(int i)
	{
		return value[i];
	}
}