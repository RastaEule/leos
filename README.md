# About
LeOS is a micro OS based on the [SJC Compiler by Prof. Dr. Stefan Frenz](http://www.fam-frenz.de/stefan/compiler.html) and is written in a dialect of java. It is working in 32 bit protected mode and is quite basic. It was built during the course "Betriebssysteme im Eigenbau" at the University of Applied Sciences Kempten in the summer term 2021.

LeOS offers the following funcionalities:
- Basic Shell
- Scheduler for cooperative multitasking
- Memory management with garbage collection and virtual memory
- Bluescreen with call stack
- PCI scan: List all attached PCI devices
- Memory map: List all memory segments
- Game: a small doodle jump clone.

# Compiling
To compile the OS take a look at the [run.bat](run.bat) file. This repository already contains the [SJC Compiler by Prof. Dr. Stefan Frenz](http://www.fam-frenz.de/stefan/compiler.html).

Compiling is done using the following command:
`java -jar sjc.jar rte\ source\src\ -o boot -u rte -s 1M`

This will create the file [BOOT_FLP.IMG](BOOT_FLP.IMG), which can be run with an emulator.

# Running
To run LeOS, it is recommended to use the [QEMU emulator](https://www.qemu.org/). You need to copy the file [BOOT_FLP.IMG](BOOT_FLP.IMG) to the main diretory of QEMU and run with the command: `qemu-system-i386.exe -m 32 -boot a -fda BOOT_FLP.IMG -serial stdio`

# Screenshots
![Main menu](screenshots/Hauptmenue.PNG)
![Smap and Pciscan](screenshots/smap&pciscan.PNG)
![BlueScreen](screenshots/bluescreen.PNG)
![Game Main Menu](screenshots/gameMainMenu.PNG)
![Game Doodle Jump](screenshots/gameDoodleJump.PNG)
![Game Lost](screenshots/gamelost.PNG)

# License

GPLv3
