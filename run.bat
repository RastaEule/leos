@echo off
:: Base Directory
cd /d E:\Studium\Master2\BetriebssystemImEigenbau\Programmierung\SJC\leos

echo ---- Update ObjectInfo
::python UpdateObjectInfo.py

echo ---- Compile OS
echo on
::compile rte\ source\src\ -o boot
java -jar sjc.jar rte\ source\src\ -o boot -u rte -s 1M

@echo off
echo ---- Emulate with qemu
echo on
xcopy /s /y "BOOT_FLP.IMG" "..\..\qemu\"
cd "..\..\qemu"
qemu-system-i386.exe -m 32 -boot a -fda BOOT_FLP.IMG -serial stdio

:: Return to previous folder
cd ..\SJC\leos\