package kernel.scheduler;

import keyboard.KeyboardDriver;

public class TaskInput extends Task
{
	@Override
	public void run()
	{
		KeyboardDriver.processInput();
	}
}
