package kernel.scheduler;
import collections.TaskList;
import kernel.InternalClock;
import kernel.Kernel;

public class Scheduler
{
	private static TaskList _tasks;
	private static Task _taskInput;
	private static boolean _isRunning = false;
	private static long _currentTick = 0;

	public static void init()
	{
		_tasks = new TaskList();
	}

	public static void addTask(Task t, int runDelay, String name)
	{
		t.name = name;
		t.runDelay = runDelay;
		if (t instanceof TaskInput)
			_taskInput = t;
		else
			_tasks.add(t);
	}

	public static void removeTask(String name)
	{
		for(int i = 0; i < _tasks.length(); i++) {
			if (_tasks.at(i).name.equals(name)) {
				_tasks.remove(i);
				break;
			}
		}
	}

	public static void run()
	{
		long tick = -1;
		_isRunning = true;
		while(_isRunning) {
			if (InternalClock.currentTick > tick) {
				tick = InternalClock.currentTick;

				_taskInput.run();
				for (int i = 0; i < _tasks.length(); i++) {
					Task ct = _tasks.at(i);
					if (tick != 0 && (ct.runDelay <= 0 || tick % ct.runDelay == 0))
						ct.run();
				}
			}
		}
	}

	public static void stop()
	{
		_isRunning = false;
	}
}