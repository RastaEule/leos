package kernel.scheduler;

import collections.ByteRingbuffer;
import common.Console;
import common.StaticV24;
import kernel.TestObjectA;
import kernel.memory.MMU;

public class TaskBluescreenForce extends Task
{
	@Override
	public void run()
	{
		Console.println("Run TaskBluescreenForce");
		StaticV24.println("Run TaskBluescreenForce");

		TestObjectA a = TestObjectA.getNull();
		a.doSomething(); // PageFault should happen here
		ByteRingbuffer br = new ByteRingbuffer(10);
		br = null;
		br.write((byte)13);
	}
}
