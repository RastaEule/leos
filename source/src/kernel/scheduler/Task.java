package kernel.scheduler;

public abstract class Task {
	public String name;
	/** Let this task run each x ticks **/
	public int runDelay = 0;

	public abstract void run();
}