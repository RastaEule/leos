package kernel.scheduler;

import kernel.memory.GarbageCollection;

public class TaskGarbageCollection extends Task
{
	@Override
	public void run()
	{
		GarbageCollection.run();
	}
}