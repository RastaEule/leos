package kernel;

import kernel.memory.MMU;
import kernel.scheduler.Scheduler;
import keyboard.KeyboardDriver;
import common.*;
import java.lang.*;

public class Interrupts
{
	private final static int MASTER = 0x20, SLAVE = 0xA0;

	private static long[] _intrTable;

	public static void loadInterruptDescriptorTable()
	{
		if (_intrTable == null)
		{
			Interrupts.initPic();

			_intrTable = new long[48];
			for (int i = 0; i < 48; i++)
			{
				int addr = getHandlerAddr(i);
				_intrTable[i] = buildInterruptDescriptor(addr);
			}
		}

		int baseAddress = MAGIC.addr(_intrTable[0]); // Startadresse der Table
		int tableLimit = _intrTable.length * 8 - 1; // Anzahl der Bytes im Table - 1
		long tmp=(((long)baseAddress)<<16)|(long)tableLimit;
		MAGIC.inline(0x0F, 0x01, 0x5D); MAGIC.inlineOffset(1, tmp); // lidt [ebp-0x08/tmp]
	}

	public static void loadInterruptDescriptorTableRM()
	{
		Interrupts.initPic();

		int baseAddress = 0; // Startadresse der Table
		int tableLimit = 1024; // Anzahl der Bytes im Table - 1
		long tmp=(((long)baseAddress)<<16)|(long)tableLimit;
		MAGIC.inline(0x0F, 0x01, 0x5D); MAGIC.inlineOffset(1, tmp); // lidt [ebp-0x08/tmp]
	}

	public static void toggleHWInterrupts(boolean toggle)
	{
		if (toggle)
		{
			Console.println("Enable Hardware Interrupts...");
			MAGIC.inline(0xFB);
		}
		else
		{
			Console.println("Disable Hardware Interrupts...");
			MAGIC.inline(0xFA);
		}
	}

	private static int getHandlerAddr(int index)
	{
		int classAddr = MAGIC.cast2Ref(MAGIC.clssDesc("Interrupts"));
		int codeOffset = 0;

		switch (index)
		{
			case 0:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptDivideError");
				break;
			case 1:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptDebugException");
				break;
			case 2:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptNMI");
				break;
			case 3:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptBreakpointException");
				break;
			case 4:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptINTOverflow");
				break;
			case 5:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptIndexOutOfRange");
				break;
			case 6:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptInvalidOpcode");
				break;
			case 8:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptDoubleFault");
				break;
			case 13:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptGeneralProtectionError");
				break;
			case 14:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptPageFault");
				break;
			case 7: case 9: case 10: case 11: case 12: case 15: case 16: case 17: case 18: case 19: case 20:
			case 21: case 22: case 23: case 24: case 25: case 26: case 27: case 28: case 29: case 30: case 31:
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptReserved");
				break;
			case 32: // IRQ0
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptIRQ0Timer");
				break;
			case 33: // IRQ1
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptIRQ1Keyboard");
				break;
			case 35: case 36: case 37: case 38: case 39: // IRQ 3-7
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptIRQMaster");
				break;
			case 34: case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47: // IRQ2 + IRQ8-15
				codeOffset = MAGIC.mthdOff("Interrupts", "handleInterruptIRQSlave");
				break;
		}
		return MAGIC.rMem32(classAddr + codeOffset) + MAGIC.getCodeOff();
	}

	private static long buildInterruptDescriptor(int handlerAddr)
	{
		// Split handlerAddr in two parts
		long offset1 = handlerAddr & 0xFFFF;
		long offset2 = (handlerAddr & 0xFFFF0000) >> 16;
		long deskriptor = offset1;

		int segmentSelector = 8 << 16;
		deskriptor |= segmentSelector;

		long settings = 156130651144192L;
		deskriptor |= settings;

		offset2 = offset2 << 48;
		deskriptor |= offset2;

		return deskriptor;
	}

	public static void initPic() {
		programmChip(MASTER, 0x20, 0x04); //init offset and slave config of master
		programmChip(SLAVE, 0x28, 0x02); //init offset and slave config of slave
	}

	private static void programmChip(int port, int offset, int icw3) {
		MAGIC.wIOs8(port++, (byte)0x11); // ICW1
		MAGIC.wIOs8(port, (byte)offset); // ICW2
		MAGIC.wIOs8(port, (byte)icw3); // ICW3
		MAGIC.wIOs8(port, (byte)0x01); // ICW4
	}

	// Interrupt Handlers:
	@SJC.Interrupt
	private static void handleInterruptReserved()
	{
		MAGIC.wMem32(0xB8000, -1);
		Bluescreen.displayBluescreen("ReservedInterrupt");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptDivideError()
	{
		Console.directPrint('0', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("DivideError");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptDebugException()
	{
		Console.directPrint('1', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("DebugException");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptNMI()
	{
		Console.directPrint('2', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("NMI");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptBreakpointException()
	{
		Console.directPrint('3', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("BreakpointException");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptINTOverflow()
	{
		Console.directPrint('4', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("INTOverflow");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptIndexOutOfRange()
	{
		Console.directPrint('5', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("IndexOutOfRange");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptInvalidOpcode()
	{
		Console.directPrint('6', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("InvalidOpcode");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptDoubleFault()
	{
		Console.directPrint('9', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("DoubleFault");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptGeneralProtectionError()
	{
		Console.directPrint('1', 0, 24, Console.Color.Gray, Console.Color.Red);
		Console.directPrint('3', 0, 24, Console.Color.Gray, Console.Color.Red);
		Bluescreen.displayBluescreen("GeneralProtectionError");
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptPageFault(int errorCode)
	{
		StaticV24.indent = 0;
		StaticV24.println();
		StaticV24.println("PageFault!");

		int pageTableEntry = MMU.getCR2();
		String pageTableFault = Convert.toString((long)pageTableEntry & 0xffffffffL, 16); // Get uint value by converting to long
		if (errorCode == 0) {
			Bluescreen.displayBluescreen("PageFault: Page was not present. Faulty PageTable: 0x".add(pageTableFault));
		}
		else {
			Bluescreen.displayBluescreen("PageFault: System tried to write on a read-only page. Faulty PageTable: 0x".add(pageTableFault));
		}

		Console.directPrint('1', 0, 24, Console.Color.Gray, Console.Color.Red);
		Console.directPrint('4', 0, 24, Console.Color.Gray, Console.Color.Red);
		while(true);
	}

	@SJC.Interrupt
	private static void handleInterruptIRQMaster()
	{
		// Bestätigung nur am Master PIC
		MAGIC.wIOs8(MASTER, (byte)0x20);
	}

	@SJC.Interrupt
	private static void handleInterruptIRQSlave()
	{
		// Bestätigung am Master und Slave PIC
		MAGIC.wIOs8(MASTER, (byte)0x20);
		MAGIC.wIOs8(SLAVE, (byte)0x20);
	}

	@SJC.Interrupt
	private static void handleInterruptIRQ0Timer()
	{
		// Bestätigung am Master PIC
		MAGIC.wIOs8(MASTER, (byte)0x20);

		//MAGIC.wMem8(0xB8000 + 1200, (byte)_currentTick);
		InternalClock.currentTick++;
	}

	@SJC.Interrupt
	private static void handleInterruptIRQ1Keyboard()
	{
		// Bestätigung am Master PIC
		MAGIC.wIOs8(MASTER, (byte)0x20);

		KeyboardDriver.handleInput();
		//Console.println("KEYBOARD");
	}
}
