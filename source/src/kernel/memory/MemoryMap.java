package kernel.memory;

import collections.MemoryBlockList;

public class MemoryMap
{
	private MemoryBlockList _memBlocks;

	public MemoryMap()
	{
		_memBlocks = new MemoryBlockList();
	}

	public MemoryBlockList getMemoryBlocks() { return _memBlocks; }
}
