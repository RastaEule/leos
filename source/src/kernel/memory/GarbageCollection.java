package kernel.memory;

import common.Console;
import common.ObjectInfo;
import common.StaticV24;
import common.math;

public class GarbageCollection
{
	private static int MAX_RELOCS = 100;
	private static int _markObjCount = 0;
	private static int _removedObjCount = 0;
	public static boolean _debugOutput = false;
	public static boolean _debugSweepOutput = false;

	public static void init()
	{
	}

	public static void run()
	{
		if (_debugOutput) {
			Console.clearScreen();
			MemoryManagement.showEmptyObjects();
			Console.print("GarbageCollection Running.");
		}

		StaticV24.print("Marking...");
		StaticV24.indent++;
		StaticV24.println();


		Object hoo = MAGIC.cast2Obj(MAGIC.rMem32(MAGIC.imageBase + 16));
		while(isRoot(hoo))
		{
			unmark(hoo);
			hoo = hoo._r_next;
		}

		int rootCount = 0;
		Object ho = MAGIC.cast2Obj(MAGIC.rMem32(MAGIC.imageBase + 16));
		while(isRoot(ho))
		{
			if (_debugOutput){
			StaticV24.print("root ");
			StaticV24.print(rootCount);
			StaticV24.print(" ");
			//StaticV24.print(ObjectInfo.getName(ho));
			StaticV24.print(" @0x");
			StaticV24.printHex(MAGIC.cast2Ref(ho), 8);
			StaticV24.print(" re:");
			StaticV24.print(ho._r_relocEntries);
			StaticV24.print(" type:");
			StaticV24.print(MAGIC.rMem32(MAGIC.cast2Ref(ho) - 4));
			}

			mark(ho);
			ho = ho._r_next;
			rootCount++;
			if (_debugOutput) StaticV24.println();
		}
		StaticV24.indent--;
		StaticV24.println();
		StaticV24.println("Marking Finished");
		StaticV24.print("Root Count: ");
		StaticV24.println(rootCount);
		StaticV24.println();

		_removedObjCount = 0;
		StaticV24.print("Sweeping...");
		StaticV24.indent++;
		StaticV24.println();

		sweep2();

		StaticV24.indent--;
		StaticV24.println();
		StaticV24.println("Sweeping Finished");
		StaticV24.print("Objects removed: ");
		StaticV24.print(_removedObjCount);

		if (_debugOutput) {
			Console.print("Done! Removed ");
			Console.print(_removedObjCount);
			Console.println(" Objects.");
			MemoryManagement.showEmptyObjects();
		}
	}

	private static void mark(Object root)
	{
		if (root != null && !root._marked)
		{
			root._marked = true;
			_markObjCount++;
			int adr = MAGIC.cast2Ref(root);
			if (_debugOutput) {
				StaticV24.print("Marking Obj @x0x");
				StaticV24.printHex(adr, 8);
				StaticV24.print(" re:");
				StaticV24.print(root._r_relocEntries);
				//if (root._r_relocEntries < 100) StaticV24.print(ObjectInfo.getName(root));
				StaticV24.indent++;
				StaticV24.println();
			}
			int relocEntries = math.min(root._r_relocEntries, MAX_RELOCS);

			for (int i = 3; i <= relocEntries; i++) {
				int nextObjAdr = MAGIC.rMem32(adr - i * MAGIC.ptrSize);
				int oType = 0;
				Object o = null;
				if (nextObjAdr >= MemoryManagement.memoryEnd) {
					if (_debugOutput){
					StaticV24.print("Adr is too big!: 0x");
					StaticV24.printHex(nextObjAdr, 8);
					StaticV24.println();}
					continue;
				}
				if (nextObjAdr > MemoryManagement.sjcImageUpperAdr) {
					oType = MAGIC.rMem32(nextObjAdr - 4);
					o = MAGIC.cast2Obj(nextObjAdr);

					if (o != null) {
						mark(o);
					}
				}
			}
			if (_debugOutput) {
				StaticV24.indent--;
				StaticV24.println();
			}
		}
	}

	private static void unmark(Object root)
	{
		if (root != null && root._marked)
		{
			root._marked = false;
			int adr = MAGIC.cast2Ref(root);
			int relocEntries = math.min(root._r_relocEntries, MAX_RELOCS);

			for (int i = 3; i <= relocEntries; i++) {
				int nextObjAdr = MAGIC.rMem32(adr - i * MAGIC.ptrSize);
				int oType = 0;
				Object o = null;
				if (nextObjAdr >= MemoryManagement.memoryEnd) {
					continue;
				}
				if (nextObjAdr > MemoryManagement.sjcImageUpperAdr) {
					oType = MAGIC.rMem32(nextObjAdr - 4);
					o = MAGIC.cast2Obj(nextObjAdr);

					if (o != null) {
						unmark(o);
					}
				}
			}
		}
	}

	private static void sweep2()
	{
		int objCount = 0;
		Object o = MemoryManagement._firstHeapObject._r_next;

		while(o != null){
			Object nextO = o._r_next;

			if (_debugOutput){
			StaticV24.print(objCount);
			StaticV24.print(" : ");}

			int objAdr = MAGIC.cast2Ref(o);
			if (_debugOutput){
			StaticV24.print(" @0x");
			StaticV24.printHex(objAdr, 8);

			//StaticV24.print(ObjectInfo.getName(MAGIC.cast2Obj(objAdr)));
			StaticV24.print(" marked:");
			StaticV24.print(o._marked);}

			if (o._marked)
				o._marked = false;
			else if (!MemoryManagement.isEmptyObject(o) && objAdr > MemoryManagement.memoryStart){
				if (_debugOutput || _debugSweepOutput){
					StaticV24.print(" Removing...");
					StaticV24.print(ObjectInfo.getName(o));
					StaticV24.print(" = ");
					if (o == null)
						StaticV24.print("null");
					else
						StaticV24.print(o._r_type.name);
				}
				MemoryManagement.removeUsedObject(o);
				_removedObjCount++;
				//if (_debugOutput) StaticV24.print("   Removed!");
			}
			if (_debugOutput) {
				StaticV24.println();
			}
			objCount++;

			o = nextO;
		}

		if (_debugOutput){
		StaticV24.print("Total checked Object Count: ");
		StaticV24.println(objCount);}
	}

	private static boolean isRoot(Object o)
	{
		if (o == null)
			return false;

		int hoAdr = MAGIC.cast2Ref(o);
		return hoAdr > MAGIC.imageBase && hoAdr < MemoryManagement.sjcImageUpperAdr;
	}
}