package kernel.memory;

public class MemoryBlock
{
	/** The start address of the block **/
	private long _blockBaseAddress = -1;
	/** The length of the block **/
	private long _blockLength = -1;
	/** Whether the block is reserved or free **/
	private int _blockType = -1;

	public MemoryBlock(long blockBaseAddress, long blockLength, int blockType)
	{
		_blockBaseAddress = blockBaseAddress;
		_blockLength = blockLength;
		_blockType = blockType;
	}

	public long getBlockBaseAdress() { return  _blockBaseAddress; }

	public long getBLockLength() { return _blockLength; }

	public int getBlockType() { return _blockType; }
}
