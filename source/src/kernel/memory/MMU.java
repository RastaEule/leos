package kernel.memory;
import common.StaticV24;
import rte.DynamicRuntime;

public class MMU
{
	private static int _baseAdr = 0x200000; // BaseAddress needs to be aligned to 4096 bytes
	private static int _firstPageAdr = 0;
	private static int _lastPageAdr = 0;

	public static void setCR3(int addr)
	{
		MAGIC.inline(0x8B, 0x45);
		MAGIC.inlineOffset(1, addr); //mov eax,[ebp+8]
		MAGIC.inline(0x0F, 0x22, 0xD8); //mov cr3,eax
	}

	public static void enableVirtualMemory()
	{
		MAGIC.inline(0x0F, 0x20, 0xC0); //mov eax,cr0
		MAGIC.inline(0x0D, 0x00, 0x00, 0x01, 0x80); //or eax,0x80010000
		MAGIC.inline(0x0F, 0x22, 0xC0); //mov cr0,eax
	}

	public static int getCR2()
	{
		int cr2 = 0;
		MAGIC.inline(0x0F, 0x20, 0xD0); //mov e/rax,cr2
		MAGIC.inline(0x89, 0x45);
		MAGIC.inlineOffset(1, cr2); //mov [ebp-4],eax
		return cr2;
	}

	public static void init()
	{
		StaticV24.print("Building Page Directory...");
		StaticV24.indent++;
		StaticV24.println();

		int totalsize = 4096 + 1024 * 4096; // PageDirectory (4096bytes) + Pagetables (1024, each one is 4096 bytes)
		int baseAdr = DynamicRuntime.assignSpecialMemory(totalsize, 4096);
		StaticV24.print("PageDirectory base Adr: 0x");
		StaticV24.printHex(baseAdr, 8);
		StaticV24.print(" - 0x");
		StaticV24.printHex(baseAdr + totalsize, 8);
		StaticV24.println();

		int currentAdr = baseAdr;

		// Step 1: Build PageDirectory
		for (int pd = 0; pd < 1024; pd++) { // Page Directories
			int pageTableAdr = (baseAdr + 4096) + (4096 * pd);
			int pageDirEntry = pageTableAdr | 0x3; // Present Bit ist nicht gesetzt
			MAGIC.wMem32(currentAdr, pageDirEntry);
			currentAdr += 4;
		}

		_firstPageAdr = currentAdr;
		// Step 2: Build PageTables
		int pageAdr = 0;
		for(int pt = 0; pt < 1024; pt++){ // Page Tables
			for (int pte = 0; pte < 1024; pte++){ // Page Table Entries
				int pageTableEntry = pageAdr | 0x3; // Present Bit ist nicht gesetzt
				MAGIC.wMem32(currentAdr, pageTableEntry);
				pageAdr += 4096;
				currentAdr += 4;
			}
		}
		_lastPageAdr = currentAdr - 4;

		StaticV24.indent--;
		StaticV24.println("Building Page Directory Done!");
		setCR3(baseAdr);
		enableVirtualMemory();
	}

	/**
	 * First and last pages need the present bit to be false. This has to be done after some other memory stuff
	 */
	public static void setFirstLastPagesNotPresent()
	{
		MAGIC.wMem32(_firstPageAdr, 2);

		// The value of lastPage ist calculated as following:
		// pageAdr = 1024 * 1024 * 4096 - 4096
		// 		(1024 PageDirectory Entries * 1024 PageTableEntries * 4096 Bytes of each PageTable Entry) - (4096 Bytes, to get to the last PageTable Entry)
		//	pageAdr | = 2
		//		The last 12 Bits of pageAdr are 0. To set the present bit, we want to the second bit to be 1, so OR with value 2 (0b10)
		MAGIC.wMem32(_lastPageAdr, 0xFFFFF002);
	}
}