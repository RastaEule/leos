package kernel.memory;

import common.Console;
import common.Convert;
import common.StaticV24;
import kernel.BIOS;
import rte.DynamicRuntime;

public class MemoryManagement
{
	public static boolean _debugOutput = false;

	public static Object _firstEmptyObject = null;
	private static Object _lastEmptyObject = null;
	public static Object _firstHeapObject = null;
	private static Object _lastHeapObject = null;

	public static boolean _initializing = true;
	public static int sjcImageUpperAdr = 0;
	public static int sjcImageSize = 0;
	/** The memory that can be used starts at this address **/
	public static int memoryStart = 0;
	public static int memoryEnd = 0;

	public static void init()
	{
		sjcImageSize = MAGIC.rMem32(MAGIC.imageBase + 4);
		sjcImageUpperAdr = MAGIC.imageBase + sjcImageSize;
		DynamicRuntime.init();

		StaticV24.print("SCJ Image: 0x");
		StaticV24.printHex(MAGIC.imageBase, 8);
		StaticV24.print(" - 0x");
		StaticV24.printHex(sjcImageUpperAdr, 8);
		StaticV24.println();

		// Stuff that needs special memory assignment needs to happen here:
		MMU.init();

		// Apply special memory assignments:
		memoryStart = DynamicRuntime.nextFreeAddress;

		// Create all EmptyObjects
		initializeEmptyObjects();

		StaticV24.print("Memory End: 0x");
		StaticV24.printHex(memoryEnd, 8);
		StaticV24.println();

		// MMU needs to set first and last page to not present after initializing the emptyObject
		// If they are set to not present directly, the OS doesnt boot
		MMU.setFirstLastPagesNotPresent();
		_initializing = false;
	}

	public static void showMemoryMap()
	{
		Console.print("| Block |  Lower Addr   |  Upper Addr   |       Type       |");
		int counter = 0;
		int blockNum = 0;
		do {
			BIOS.writeMemoryMapEntry(counter);
			counter = BIOS.regs.EBX;

			int offset = BIOS.regs.EDI & 0xFFFF; // Only use the lower 16 bits
			int baseAddress = (16 * BIOS.regs.ES) + offset; // segment * 16 + offset
			long baseAdr = MAGIC.rMem64(baseAddress); // The start address of the block
			long blockLength = MAGIC.rMem64(baseAddress + 8); // The length of the block
			int blockType = MAGIC.rMem32(baseAddress + 16); // Whether the block is reserved or free

			// Console output
			Console.println();
			Console.print("| ");
			String blockString = Convert.toString(blockNum, 10);
			Console.print(blockString);
			for (int j = blockString.length(); j < 5; j++)
				Console.print(' ');
			Console.print(" | ");

			String baseAddrString = Convert.toString(baseAdr, 16);
			Console.print(baseAddrString);
			for (int j = baseAddrString.length(); j < 13; j++)
				Console.print(' ');
			Console.print(" | ");

			String upperAddrString = Convert.toString(baseAdr + blockLength, 16);
			Console.print(upperAddrString);
			for (int j = upperAddrString.length(); j < 13; j++)
				Console.print(' ');
			Console.print(" | ");

			// Save current color value
			int fg = Console.foregroundColor;
			int bg = Console.backgroundColor;
			String typeString = "Unkown";
			switch (blockType) {
				case 1:
					Console.setColor(fg, Console.Color.Green);
					typeString = "Free";
					break;
				case 2:
					Console.setColor(fg, Console.Color.Red);
					typeString = "Reserved";
					break;
				case 3:
					typeString = "ACPI reclaimable";
					break;
				case 4:
					typeString = "ACPI NVS Memory";
					break;
				case 5:
					typeString = "Bad memory";
					Console.setColor(fg, Console.Color.Brown);
					break;
			}
			Console.print(typeString);
			for (int j = typeString.length(); j < 16; j++)
				Console.print(' ');

			Console.setColor(fg, bg); // Reset color to previous value
			Console.print(" |");
			blockNum++;
		}
		while (counter != 0);
	}

	public static void showEmptyObjects()
	{
		Console.print("SJC-Image: ");
		Console.printHex(MAGIC.imageBase);
		Console.print("\t - \t");
		Console.printHex(MAGIC.imageBase + MAGIC.rMem32(MAGIC.imageBase + 4));
		Console.println();

		Console.println("Empty Objects:");
		Object eo = _firstEmptyObject;
		int i = 0;
		int maxDisplay = 10;
		while (eo != null) {
			if (i == maxDisplay)
				Console.println(" ... etc more ...");
			if (i < maxDisplay) {
				Console.print(" | ");
				Console.print(i);
				Console.print(": ");
				Console.printHex(getObjectLowerAdress(eo));
				Console.print("\t - \t");
				Console.printHex(getObjectUpperAdress(eo));
				Console.println();
			}
			i++;
			eo = eo._r_next;
		}
		Console.print("Empty Objects count: ");
		Console.println(i);

		Object uo = _firstHeapObject;
		i = 0;
		while (uo != null) {
			i++;
			uo = uo._r_next;
		}
		Console.print("Used Objects Count: ");
		Console.println(i);
	}

	/**
	 * Returns a starting address where an object of a given size can be placed
	 *
	 * @param size
	 * @return
	 */
	public static int getObjectAddress(int size)
	{
		int adr = -1;
		Object eo = _firstEmptyObject;
		while (eo != null) {
			// Align to 4 bytes
			int eobjSize = (getObjectSize(eo) + 3) & ~3;
			int eObjMinSize = (getEmptyObjectMinSize() + 3) & ~3;
			int targetSize = (size + 3) & ~3;
			// Check if this EmptyObject is large enough
			if (targetSize <= eobjSize) // The targetObject does generally fit in the size of this EmptyObject
			{
				// TODO: Damit das EmptyObject normal geschrumpft werden kann, muss es kleiner sein als EmptyObject.scalarSize - 8
				// Die 12 muss abgezogen werden, weil die beiden ScalarEntries RelocSize, Marked und ScalarSize dürfen natürlich nicht entfernt werden.
				if (targetSize <= eobjSize - eObjMinSize + 12) // Default case: targetObject fits in the scalarSize - 12 of an EmptyObject
				{
					shrinkObject(eo, size);
					adr = getObjectUpperAdress(eo);
					break;
				}
				else // targetObject is larger than the EmptyObject can be shrinked
				{
					// Remove this EmptyObject and resize the resulting targetObject
					adr = getObjectLowerAdress(eo);
					DynamicRuntime.scalarSizeDelta = eobjSize - targetSize; // Make DynamicRuntime make the new Object a little bigger to fill the space
					removeEmptyObject(eo); // Remove references to this emptyObject
					break;
				}
			}
			eo = eo._r_next;
		}
		if (adr == -1) // Couldn't find a valid address
		{
			Console.setColor(Console.Color.Black, Console.Color.Red);
			Console.println("ERROR: COULD NOT FIND A VALID ADRESS FOR AN OBJECT");
			Console.println("Maybe there is no Space left?");
			while (true)
				;
		}
		return adr;
	}

	/**
	 * Used to make EmptyObjects smaller
	 *
	 * @param targetObj Which Object to shrink
	 * @param amount    How many bytes to shrink targetObj
	 * @return A new, smaller object
	 */
	public static void shrinkObject(Object targetObj, int amount)
	{
		if (targetObj._r_scalarSize > amount) {
			MAGIC.assign(targetObj._r_scalarSize, targetObj._r_scalarSize - amount);
		}
	}

	/**
	 * Updates the last used heapObject.
	 *
	 * @param object
	 */
	public static void updateLastObject(Object object)
	{
		_lastHeapObject = object;
		if (_firstHeapObject == null)
			_firstHeapObject = object;
	}

	/**
	 * Returns the object of the last created object.
	 *
	 * @return Object address of the last created object.
	 */
	public static Object getLastObject()
	{
		return _lastHeapObject;
	}

	public static void removeUsedObject(Object o)
	{
		Object prevObj = getPreviousUsedObject(o);
		Object nextObj = o._r_next;
		if (prevObj != null)
			MAGIC.assign(prevObj._r_next, nextObj);

		if(_debugOutput){
			StaticV24.print("rmobj@ 0x");
			StaticV24.printHex(MAGIC.cast2Ref(o), 8);
			StaticV24.print(" ");
			//StaticV24.print(ObjectInfo.getName(o));

			//Console.print(" | lowAdr: ");
			//Console.printHex(getObjectLowerAdress(o));
			StaticV24.print(" size: 0x");
			StaticV24.printHex(getObjectSize(o), 8);
			//Console.print(" ss: ");
			//Console.print(o._r_scalarSize);
			StaticV24.print(" re: ");
			StaticV24.print(o._r_relocEntries);
		}

		int lowAdr = getObjectLowerAdress(o);
		int size = getObjectSize(o);
		for (int j = lowAdr; j < lowAdr+size; j+=4) {
			MAGIC.wMem32(j, 0);
		}
		if (size > 0)
			addEmptyObject(lowAdr, size);
		else
			StaticV24.print(" Size is too low!");
	}

	/**
	 * Creates initial EmptyObjects in free memory areas above the base image.
	 */
	private static void initializeEmptyObjects()
	{
		StaticV24.print("initializeEmptyObjects starting");

		int counter = 0;
		do {
			BIOS.writeMemoryMapEntry(counter);
			counter = BIOS.regs.EBX;

			int offset = BIOS.regs.EDI & 0xFFFF; // Only use the lower 16 bits
			int baseAddress = (16 * BIOS.regs.ES) + offset; // segment * 16 + offset
			long blockBaseAdr = MAGIC.rMem64(baseAddress); // The start address of the block
			long blockLength = MAGIC.rMem64(baseAddress + 8); // The length of the block

			int blockType = MAGIC.rMem32(baseAddress + 16); // Whether the block is reserved or free
			if (blockType == 1 && blockBaseAdr >= MAGIC.imageBase) { // Only consider blocks above ImageBase
				int upperAdr = (int) blockBaseAdr + (int) blockLength;
				if (blockBaseAdr < memoryStart) { // Consider the SJC-Image
					blockBaseAdr = memoryStart;
					memoryEnd = (int)(blockBaseAdr + blockLength);
				}
				int size = upperAdr - (int) blockBaseAdr;
				addEmptyObject((int) blockBaseAdr, size);
				//Console.println("Created empty object");
			}
		}
		while (counter != 0);

		StaticV24.println("initializeEmptyObjects finished");
	}

	/**
	 * Adds a new EmptyObject to the list.
	 *
	 * @param startAddress Start address of the new EmptyObect.
	 * @param size         Size of the new EmptyObject.
	 */
	private static void addEmptyObject(int startAddress, int size)
	{
		// Step 1: check if this EmptyObject can be merged with another one
		if (!tryMergeEmptyObject(startAddress, size)){ // TODO: Try to merge emptyObjects
			Object emptyObject = DynamicRuntime.newEmptyObject(startAddress, size);

			if (_firstEmptyObject == null) {
				_firstEmptyObject = emptyObject;
			}
			if (_lastEmptyObject != null) {
				MAGIC.assign(_lastEmptyObject._r_next, emptyObject);
			}
			_lastEmptyObject = emptyObject;
		}
	}

	private static void removeEmptyObject(Object o)
	{
		Object prevObj = getPreviousEmptyObject(o);
		if (prevObj == null)
			_firstEmptyObject = o._r_next;
		else
			MAGIC.assign(prevObj._r_next, o._r_next);
	}

	/**
	 * When trying to create a new EmptyObject with startAdr and size, check if this space can be used by other Emptyobjects first
	 * @param startAdr The Start Address of the new EmptyObject to be created
	 * @param size The size of the new EmptyObject to be created
	 * @return true, if no new EmptyObject needs to be created, because the space can be used by another EmptyObject
	 */
	private static boolean tryMergeEmptyObject(int startAdr, int size)
	{
		if (_debugOutput) StaticV24.print("Try Merging EmptyObjects..");

		Object prevEO = null;
		Object nextEO = null;
		Object o = _firstEmptyObject;
		while(o != null){
			int upperAdr = getObjectUpperAdress(o);
			int lowerAdr = getObjectLowerAdress(o);
			if (lowerAdr < memoryStart ||lowerAdr >= upperAdr)
				break;

			if (upperAdr == startAdr)
				prevEO = o;
			else if (lowerAdr == startAdr + size)
				nextEO = o;
			o = o._r_next;
		}
		if (_debugOutput){
		StaticV24.print(".. prevO:");
		StaticV24.print(prevEO==null);
		StaticV24.print(" nextO:");
		StaticV24.print(nextEO==null);}

		if (prevEO != null && nextEO != null) { // Merge prevEO, thisEO and nextEO
			if (_debugOutput) StaticV24.println(" Merge Empty Objects case 1");
			MAGIC.assign(prevEO._r_next, nextEO._r_next);
			MAGIC.assign(prevEO._r_scalarSize, prevEO._r_scalarSize + getObjectSize(nextEO) + size);
			return true;
		}
		else if (prevEO != null && nextEO == null){ // Merge prevEO and thisEO
			if (_debugOutput) StaticV24.println("Merge Empty Objects case 2");
			MAGIC.assign(prevEO._r_scalarSize, prevEO._r_scalarSize + size);
			return true;
		}
		else if (prevEO == null && nextEO != null){ // Create new eo and remove nexteo
			if (_debugOutput) StaticV24.println("Merge Empty Objects case 3");
			Object newEO = DynamicRuntime.newEmptyObject(startAdr, getObjectSize(nextEO) + size);
			MAGIC.assign(newEO._r_next, nextEO._r_next);

			Object keo = _firstEmptyObject;
			while (keo != null){
				if (keo._r_next == nextEO){
					MAGIC.assign(keo._r_next, newEO);
					break;
				}
				keo = keo._r_next;
			}
			return true;
		}
		if (_debugOutput) StaticV24.println(" Merging impossible ");
		return false;
	}

	private static Object moveEmptyObject(Object o, int newLowerAdr)
	{
		Object prevO = getPreviousEmptyObject(o);
		Object nextO = o._r_next;
		int relocEntries = o._r_relocEntries;

		int writePos = newLowerAdr;
		for(int readPos = getObjectLowerAdress(o); readPos < getObjectUpperAdress(o); readPos+=4){
			MAGIC.wMem32(writePos, readPos);
			writePos+=4;
		}
		Object newO = MAGIC.cast2Obj(newLowerAdr + relocEntries * MAGIC.ptrSize);

		// Reset references, I dont know if this is necessary
		MAGIC.assign(prevO._r_next, newO);
		MAGIC.assign(newO._r_next, nextO);
		return newO;
	}

	private static Object getPreviousEmptyObject(Object o)
	{
		Object prevObj = null;
		Object nextObj = _firstEmptyObject;
		while (nextObj != null) {
			// Get the previous Object of emptyObject
			if (nextObj._r_next == o) {
				prevObj = nextObj;
				break;
			}
			nextObj = nextObj._r_next;
		}
		return prevObj;
	}

	private static Object getPreviousUsedObject(Object o)
	{
		Object prevObj = null;
		Object nextObj = _firstHeapObject;
		while (nextObj != null) {
			// Get the previous Object of emptyObject
			if (nextObj._r_next == o) {
				prevObj = nextObj;
				break;
			}
			nextObj = nextObj._r_next;
		}
		return prevObj;
	}

	public static boolean isEmptyObject(Object o)
	{
		int adrO = MAGIC.cast2Ref(o);

		Object eo = _firstEmptyObject;
		while (eo != null){
			int adrEO = MAGIC.cast2Ref(eo);
			if (adrO == adrEO)
				return true;
			eo = eo._r_next;
		}
		return false;
	}

	/**
	 * Returns the lowest possible size of an EmptyObject
	 */
	private static int getEmptyObjectMinSize() { return MAGIC.getInstScalarSize("EmptyObject") + MAGIC.getInstRelocEntries("EmptyObject") * 4;}

	private static int getObjectUpperAdress(Object o) { return MAGIC.cast2Ref(o) + o._r_scalarSize; }

	private static int getObjectLowerAdress(Object o) { return MAGIC.cast2Ref(o) - o._r_relocEntries * 4;}

	private static int getObjectSize(Object o) { return getObjectUpperAdress(o) - getObjectLowerAdress(o); }
}