package kernel;

import common.Console;
import common.Convert;

public class PCIBus
{
	/**
	 * Perform a scan on the PCI Bus and display all devices
	 */
	public static void performPCIBusScan()
	{
		Console.println("Bus | Device | Function |    BaseClass    | Subclass |  VendorID  |  DeviceID ");

		for (int busNumber = 0; busNumber < 256; busNumber++)
		{
			for (int deviceNumber = 0; deviceNumber < 32; deviceNumber++)
			{
				int funCount = 1;
				int data = getData(3, 0, deviceNumber, busNumber);
				int header = (data & 0xFF0000) >> 16;
				if ((header >> 7) == 1)
					funCount = 8;

				for (int funNumber = 0; funNumber < funCount; funNumber++)
				{
					data = getData(0, 0, deviceNumber, busNumber);
					if (data != 0 && data != -1) // Check if device is valid
					{
						int vendorID = data & 0xFFFF;
						int deviceID = (data & 0xFFFF0000) >> 16;

						data = getData(2, funNumber, deviceNumber, busNumber);
						int subClassCode = (data & 0xFF0000) >> 16;
						int baseClassCode = ((data & 0xFF000000) >> 24);

						if (baseClassCode != -1)
							printDevice(busNumber, deviceNumber, funNumber, baseClassCode, subClassCode, vendorID, deviceID);
					}
				}
			}
		}
	}

	private static int getData(int register, int function, int deviceNumber, int busNumber)
	{
		int command = 0;
		command = register << 2;
		command |= function << 8;
		command |= deviceNumber << 11;
		command |= busNumber << 16;
		command |= 0x80 << 24;

		MAGIC.wIOs32(0x0CF8, command);
		return MAGIC.rIOs32(0x0CFC);
	}

	private static void printDevice(int busNumber, int deviceNumber, int function, int baseClass, int subClass, int vendorID, int deviceID)
	{

		String s = Convert.toString(busNumber, 16);
		Console.print(s);
		for (int i = s.length(); i < 3; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(deviceNumber, 16);
		Console.print(s);
		for (int i = s.length(); i < 6; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(function, 16);
		Console.print(s);
		for (int i = s.length(); i < 8; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(baseClass, 16);
		String baseClassStr = "";
		switch (baseClass)
		{
			case 0x00: baseClassStr = "Old Device"; break;
			case 0x01: baseClassStr = "Mass Storage"; break;
			case 0x02: baseClassStr = "NetworkCtrl"; break;
			case 0x03: baseClassStr = "DisplCtrl"; break;
			case 0x04: baseClassStr = "Multimedia"; break;
			case 0x05: baseClassStr = "MemoryCtrl"; break;
			case 0x06: baseClassStr = "Bridge"; break;
			case 0x07: baseClassStr = "CommCtrl"; break;
			case 0x08: baseClassStr = "SysPeriphery"; break;
			case 0x09: baseClassStr = "Input Device"; break;
			case 0x0A: baseClassStr = "DckingStation"; break;
			case 0x0B: baseClassStr = "ProcessorUnit"; break;
			case 0x0C: baseClassStr = "Serial Bus"; break;
			case 0x0D: baseClassStr = "WirelessCom"; break;
			case 0x0E: baseClassStr = "IntelliCtrl"; break;
			case 0x0F: baseClassStr = "SatelliteComm"; break;
		}

		Console.print(s);
		Console.print(' ');
		Console.print(baseClassStr);
		for (int i = s.length() + baseClassStr.length() + 1; i < 15; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(subClass, 16);
		Console.print(s);
		for (int i = s.length(); i < 8; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(vendorID, 16);
		Console.print(s);
		for (int i = s.length(); i < 10; i++)
			Console.print(' ');
		Console.print(" | ");

		s = Convert.toString(deviceID, 16);
		Console.print(s);
		for (int i = s.length(); i < 10; i++)
			Console.print(' ');

		Console.println();
	}
}