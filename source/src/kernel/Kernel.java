package kernel;

import collections.IntRingBuffer;
import kernel.memory.*;
import kernel.scheduler.*;
import keyboard.*;
import common.*;

public class Kernel
{
	public static void main()
	{
		//MAGIC.wMem32(0xB8000, -1);
		printTitleScreen();
		init();

		Scheduler.addTask(new TaskInput(), 0, "UserInput");
		Scheduler.addTask(new TaskGarbageCollection(), 50, "GarbageCollection");
		//Scheduler.addTask(new TaskBluescreenForce(), 60, "TaskBluescreenForce");
		Scheduler.run();
	}

	private static void testBlueScreen()
	{
		MAGIC.inline(0xCC);
	}

	/**
	 * Initialize all necessary stuff for the OS to work
	 */
	private	static void init()
	{
		StaticV24.println("=============================================================");
		StaticV24.println("Booting LeOS ================================================");
		StaticV24.println("=============================================================");

		Console.println("Do some MemoryManagement...");
		MemoryManagement.init();

		Console.println("Initialize the Keyboard Driver...");
		KeyboardDriver.init(100, false);

		Console.println("Setting up Interrupt Table...");
		Interrupts.loadInterruptDescriptorTable();
		Interrupts.toggleHWInterrupts(true);

		KeypressHandler.init();
		KeypressHandler.registerInputHandler(new Shell());

		Scheduler.init();
		GarbageCollection.init();

		Console.setColor(Console.Color.Black, Console.Color.Green);
		Console.println("Booting Successful!");
		Console.setColor(Console.Color.Gray, Console.Color.Black);
		Console.println("Type \"help\" for possible commands");
		Console.setColor(Console.Color.Black, Console.Color.Turquoise);
	}

	private static void printTitleScreen()
	{
		Console.clearScreen();
		Console.setColor(Console.Color.Turquoise, Console.Color.Blue);
		Console.setCursor(0, 0);
		/*
		String a = "*==============================================================================*";
		Console.println(a);
		a = "=================================  Welcome To  =================================";
		Console.println(a);
		a = "*==============================================================================*";
		Console.println(a);
		a = "||||||||  88                        ,ad8888ba,     ad88888ba   ----------|||||||";
		Console.println(a);
		a = "||||||||  88                       d8**    **8b   d8*     *8b  ----------|||||||";
		Console.println(a);
		a = "||||||||  88                      d8*        *8b  Y8,          ----------|||||||";
		Console.println(a);
		a = "||||||||  88           ,adPPYba,  88          88  *Y8aaaaa,    ----------|||||||";
		Console.println(a);
		a = "||||||||  88          a8P_____88  88          88    *****T8b,  ----------|||||||";
		Console.println(a);
		a = "||||||||  88          8PPPTTT***  Y8,        ,8P          `8b  ----------|||||||";
		Console.println(a);
		a = "||||||||  88          8PP**       Y8,        ,8P          `8b  ----------|||||||";
		Console.println(a);
		a = "||||||||  88          *8b,   ,aa   Y8a.    .a8P   Y8a     a8P  ----------|||||||";
		Console.println(a);
		a = "||||||||  88888888888  **Ybbd8**    **Y8888Y**     *Y88888P*   ----------|||||||";
		Console.println(a);
		a = "*==============================================================================*";
		Console.println(a);
		a = "================================================================================";
		Console.println(a);
		a = "*==============================================================================*";
		Console.println(a);

		 */


		// Alternative Anzeige mit ASCII-Zeichen:

		// Zeile 1
		Console.print((char)201);
		for (int i = 0; i < 25; i++)
			Console.print((char)205);
		Console.println((char)187);

		// Zeile 2
		Console.print((char)186);
		Console.print(" ");
		Console.print((char)176);
		Console.print((char)177);
		Console.print((char)178);
		Console.print(" Welcome to LeOS ");
		Console.print((char)178);
		Console.print((char)177);
		Console.print((char)176);
		Console.print(" ");
		Console.println((char)186);

		// Zeile 3
		Console.print((char)200);
		for (int i = 0; i < 25; i++)
			Console.print((char)205);
		Console.println((char)188);

		Console.setColor(Console.Color.Gray, Console.Color.Black);
	}

	public static void testGraphicMode()
	{
		// Switch to graphic mode 320x200 pixels
		BIOS.setGraphicMode320x200();

		int baseAdr = 0xA0000;
		int color = 0x00;
		// Paint something
		for (int i = 0; i < 320 * 200; i++)
		{
			MAGIC.wMem8(baseAdr++, (byte)'+');
			MAGIC.wMem8(baseAdr++, (byte) color);
			color = (color + 1) % 0xFF;
		}

		// Wait a while using the timer
		long timestamp = InternalClock.currentTick;
		do {
		} while (InternalClock.currentTick < timestamp + 50);

		// Switch to graphic mode 80x25 pixels
		BIOS.setGraphicMode80x25();
		Console.setCursor(0, 0);
	}
}