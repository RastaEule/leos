package keyboard;

public interface IKeyboardInputReceiver
{
	/**
	 * Event that a key has been pressed
	 * @param keyCode which key has been pressed
	 */
	void onKeyDown(int keyCode);

	/**
	 * Event that a key has been lifted from the keyboard
	 * @param keyCode Which key has been lifted
	 */
	void onKeyUp(int keyCode);

	String getName();
}
