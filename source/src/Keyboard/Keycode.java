package keyboard;

/**
 * Maps all possible Keyboard Keys to integer values.
 */
public class Keycode
{
	/**
	 * Get a character from a keycode. Will return a result only if the keyCode can be displayed
	 */
	public static char getChar(int keyCode)
	{
		char c = (char)219;
		switch (keyCode)
		{
			case Keycode.Space: c = ' '; break;
			case Keycode.Enter: c = '\n'; break;
			case Keycode.n1: case Keycode.NUM_1: c = '1'; break; // Numbers
			case Keycode.n2: case Keycode.NUM_2: c = '2'; break;
			case Keycode.n3: case Keycode.NUM_3: c = '3'; break;
			case Keycode.n4: case Keycode.NUM_4: c = '4'; break;
			case Keycode.n5: case Keycode.NUM_5: c = '5'; break;
			case Keycode.n6: case Keycode.NUM_6: c = '6'; break;
			case Keycode.n7: case Keycode.NUM_7: c = '7'; break;
			case Keycode.n8: case Keycode.NUM_8: c = '8'; break;
			case Keycode.n9: case Keycode.NUM_9: c = '9'; break;
			case Keycode.n0: case Keycode.NUM_0: c = '0'; break;
			case Keycode.Q: c = 'Q'; break; // Big letters
			case Keycode.W: c = 'W'; break;
			case Keycode.E: c = 'E'; break;
			case Keycode.R: c = 'R'; break;
			case Keycode.T: c = 'T'; break;
			case Keycode.Z: c = 'Z'; break;
			case Keycode.U: c = 'U'; break;
			case Keycode.I: c = 'I'; break;
			case Keycode.O: c = 'O'; break;
			case Keycode.P: c = 'P'; break;
			case Keycode.Ger_UE: c = 'U'; break;
			case Keycode.A: c = 'A'; break;
			case Keycode.S: c = 'S'; break;
			case Keycode.D: c = 'D'; break;
			case Keycode.F: c = 'F'; break;
			case Keycode.G: c = 'G'; break;
			case Keycode.H: c = 'H'; break;
			case Keycode.J: c = 'J'; break;
			case Keycode.K: c = 'K'; break;
			case Keycode.L: c = 'L'; break;
			case Keycode.Ger_OE: c = 'O'; break;
			case Keycode.Ger_AE: c = 'A'; break;
			case Keycode.Y: c = 'Y'; break;
			case Keycode.X: c = 'X'; break;
			case Keycode.C: c = 'C'; break;
			case Keycode.V: c = 'V'; break;
			case Keycode.B: c = 'B'; break;
			case Keycode.N: c = 'N'; break;
			case Keycode.M: c = 'M'; break;
			case Keycode.q: c = 'q'; break; // Small letters
			case Keycode.w: c = 'w'; break;
			case Keycode.e: c = 'e'; break;
			case Keycode.r: c = 'r'; break;
			case Keycode.t: c = 't'; break;
			case Keycode.z: c = 'z'; break;
			case Keycode.u: c = 'u'; break;
			case Keycode.i: c = 'i'; break;
			case Keycode.o: c = 'o'; break;
			case Keycode.p: c = 'p'; break;
			case Keycode.Ger_UEsmall: c = 'u'; break;
			case Keycode.a: c = 'a'; break;
			case Keycode.s: c = 's'; break;
			case Keycode.d: c = 'd'; break;
			case Keycode.f: c = 'f'; break;
			case Keycode.g: c = 'g'; break;
			case Keycode.h: c = 'h'; break;
			case Keycode.j: c = 'j'; break;
			case Keycode.k: c = 'k'; break;
			case Keycode.l: c = 'l'; break;
			case Keycode.Ger_OEsmall: c = 'o'; break;
			case Keycode.Ger_AEsmall: c = 'a'; break;
			case Keycode.y: c = 'y'; break;
			case Keycode.x: c = 'x'; break;
			case Keycode.c: c = 'c'; break;
			case Keycode.v: c = 'v'; break;
			case Keycode.b: c = 'b'; break;
			case Keycode.n: c = 'n'; break;
			case Keycode.m: c = 'm'; break;
			case Keycode.Tilde: c = '~'; break; // Symbols
			case Keycode.BackQuote: c = '`'; break;
			case Keycode.ExclamationMark: c = '!'; break;
			case Keycode.At: c = '@'; break;
			case Keycode.Hash: c = '#'; break;
			case Keycode.Dollar: c = '$'; break;
			case Keycode.Percent: c = '%'; break;
			case Keycode.Degree: c = (char)248; break;
			case Keycode.Caret: c = '^'; break;
			case Keycode.And: c = '&'; break;
			case Keycode.Asterisk: case Keycode.NUM_Asterisk: c = '*'; break;
			case Keycode.LeftParanthesis: c = '('; break;
			case Keycode.RightParanthesis: c = ')'; break;
			case Keycode.Minus: case Keycode.NUM_Minus: c = '-'; break;
			case Keycode.Underscore: c = '_'; break;
			case Keycode.Plus: case Keycode.NUM_Plus: c = '+'; break;
			case Keycode.Equal: c = '='; break;
			case Keycode.LeftCurlyBracket: c = '{'; break;
			case Keycode.RightCurlyBracket: c = '}'; break;
			case Keycode.LeftBracket: c = '['; break;
			case Keycode.RightBracket: c = ']'; break;
			case Keycode.VerticalBar: c = '|'; break;
			case Keycode.Backslash: c = '\\'; break;
			case Keycode.Slash: case Keycode.NUM_Slash: c = '/'; break;
			case Keycode.Colon: c = ':'; break;
			case Keycode.Semicolon: c = ';'; break;
			case Keycode.QuotationMark: c = '"'; break;
			case Keycode.Apostrophe: c = '\''; break;
			case Keycode.LessThan: c = '<'; break;
			case Keycode.GreaterThan: c = '>'; break;
			case Keycode.Comma: case Keycode.NUM_Comma: c = ','; break;
			case Keycode.Period: c = '.'; break;
			case Keycode.QuestionMark: c = '?'; break;
			case Keycode.Pause: c = 'p'; break; // TODO: Remove this line, just testing
		}
		return c;
	}

	/**
	 * Can a keyCode be represented as char?
	 * e.g. 'A', '1', can be displayed; 'ALT, 'CTRL' not
	 * @param keyCode
	 * @return
	 */
	public static boolean isPrintable(int keyCode)
	{
		boolean result = getChar(keyCode) != (char)219;
		if (result)
		{
			// Special cases:
			// Characters that return a valid char, but aren't really printable
			if (keyCode == Keycode.Enter ||
				keyCode == Keycode.Tab)
				return false;
		}
		return result;
	}

	// Specials:
	public static final int Escape = 0;
	/** Windows Key */
	public static final int Command = 1; // e.g. Windows Key
	public static final int Backspace = 2;
	public static final int Enter = 3;
	public static final int Tab = 4;
	public static final int Shift = 5;
	/** Ctrl */
	public static final int Control = 6;
	public static final int Alt = 7;
	public static final int AltGr = 8;
	public static final int Space = 9;
	public static final int ArrowLeft = 10;
	public static final int ArrowRight = 11;
	public static final int ArrowUp = 12;
	public static final int ArrowDown = 13;
	public static final int Delete = 14;
	public static final int Insert = 15;
	public static final int Pos1 = 16;
	public static final int End = 17;
	public static final int PageUp = 18;
	public static final int PageDown = 19;
	public static final int CapsLock = 20;
	public static final int NumLock = 21;
	public static final int Print = 22;
	public static final int Roll = 23;
	public static final int Pause = 24;
	public static final int Break = 25;
	public static final int F1 = 26;
	public static final int F2 = 27;
	public static final int F3 = 28;
	public static final int F4 = 29;
	public static final int F5 = 30;
	public static final int F6 = 31;
	public static final int F7 = 32;
	public static final int F8 = 33;
	public static final int F9 = 34;
	public static final int F10 = 35;
	public static final int F11 = 36;
	public static final int F12 = 37;

	// Numbers:
	public static final int n1 = 38;
	public static final int n2 = 39;
	public static final int n3 = 40;
	public static final int n4 = 41;
	public static final int n5 = 42;
	public static final int n6 = 43;
	public static final int n7 = 44;
	public static final int n8 = 45;
	public static final int n9 = 46;
	public static final int n0 = 47;

	// Letters:
	public static final int Q = 48;
	public static final int W = 49;
	public static final int E = 50;
	public static final int R = 51;
	public static final int T = 52;
	public static final int Z = 53;
	public static final int U = 54;
	public static final int I = 55;
	public static final int O = 56;
	public static final int P = 57;
	/** Ü */
	public static final int Ger_UE = 58; // Ü
	public static final int A = 59;
	public static final int S = 60;
	public static final int D = 61;
	public static final int F = 62;
	public static final int G = 63;
	public static final int H = 64;
	public static final int J = 65;
	public static final int K = 66;
	public static final int L = 67;
	/** Ö */
	public static final int Ger_OE = 68; // Ö
	/** Ä */
	public static final int Ger_AE = 69; // Ä
	public static final int Y = 70;
	public static final int X = 71;
	public static final int C = 72;
	public static final int V = 73;
	public static final int B = 74;
	public static final int N = 75;
	public static final int M = 76;

	// Small Letters:
	public static final int q = 77;
	public static final int w = 78;
	public static final int e = 79;
	public static final int r = 80;
	public static final int t = 81;
	public static final int z = 82;
	public static final int u = 83;
	public static final int i = 84;
	public static final int o = 85;
	public static final int p = 86;
	/** ü */
	public static final int Ger_UEsmall = 87; // ü
	public static final int a = 88;
	public static final int s = 89;
	public static final int d = 90;
	public static final int f = 91;
	public static final int g = 92;
	public static final int h = 93;
	public static final int j = 94;
	public static final int k = 95;
	public static final int l = 96;
	/** ö */
	public static final int Ger_OEsmall = 97;
	/** ä */
	public static final int Ger_AEsmall = 98;
	public static final int y = 99;
	public static final int x = 100;
	public static final int c = 101;
	public static final int v = 102;
	public static final int b = 103;
	public static final int n = 104;
	public static final int m = 105;
	// Symbols:
	/** ~ */
	public static final int Tilde = 106; // ~
	/** ` */
	public static final int BackQuote = 107; // `
	/** ! */
	public static final int ExclamationMark = 108; // !
	/** @ */
	public static final int At = 109; // @
	/** # */
	public static final int Hash = 110; // #
	/** € */
	public static final int Euro = 111; // €
	/** $ */
	public static final int Dollar = 112; // $
	/** § */
	public static final int Paragraph = 113; // §
	/** % */
	public static final int Percent = 114; // %
	/** ° */
	public static final int Degree = 115; // °
	/** ^ */
	public static final int Caret = 116; // ^
	/** & */
	public static final int And = 117; // &
	/** * */
	public static final int Asterisk = 118; // *
	/** ( */
	public static final int LeftParanthesis = 119; // (
	/** ) */
	public static final int RightParanthesis = 120; // )
	/** - */
	public static final int Minus = 121; // -
	/** _ */
	public static final int Underscore = 122; // _
	/** + */
	public static final int Plus = 123; // +
	/** = */
	public static final int Equal = 124; // =
	/** { */
	public static final int LeftCurlyBracket = 125; // {
	/** } */
	public static final int RightCurlyBracket = 126; // }
	/** [ */
	public static final int LeftBracket = 127; // [
	/** ] */
	public static final int RightBracket = 128; // ]
	/** | */
	public static final int VerticalBar = 129; // |
	/** \ */
	public static final int Backslash = 130; // \
	/** / */
	public static final int Slash = 131; // /
	/** : */
	public static final int Colon = 132; // :
	/** ; */
	public static final int Semicolon = 133; // ;
	/** " */
	public static final int QuotationMark = 134; // "
	/** ' */
	public static final int Apostrophe = 135; // '
	/** < */
	public static final int LessThan = 136; // <
	/** > */
	public static final int GreaterThan = 137; // >
	/** , */
	public static final int Comma = 138; // ,
	/** . */
	public static final int Period = 139; // .
	/** ? */
	public static final int QuestionMark = 140; // ?

	// Numpad:
	public static final int NUM_Slash = 141;
	public static final int NUM_Asterisk = 142;
	public static final int NUM_Minus = 143;
	public static final int NUM_Plus = 144;
	public static final int NUM_Enter = 145;
	public static final int NUM_Comma = 146;
	public static final int NUM_0 = 147;
	public static final int NUM_1 = 148;
	public static final int NUM_2 = 149;
	public static final int NUM_3 = 150;
	public static final int NUM_4 = 151;
	public static final int NUM_5 = 152;
	public static final int NUM_6 = 153;
	public static final int NUM_7 = 154;
	public static final int NUM_8 = 155;
	public static final int NUM_9 = 156;

	// Additional Stuff I initially forgot:
	public static final int RControl = 157;
	public static final int RShift = 158;
	/** ß */
	public static final int Ger_SharpS = 159;
	/** ´ */
	public static final int AcuteAccent = 160;
}