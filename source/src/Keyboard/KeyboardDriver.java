package keyboard;

import collections.IntRingBuffer;
import common.Console;


public class KeyboardDriver
{
	public static IntRingBuffer _buffer; //TODO: make private again

	private static boolean _displayDebugInfo = false;

	/**
	 * After an E1-Signal, 2 other signals will follow. This variable keeps track, which signal is the current one
	 */
	private static int _currentE1Pos = -1;

	public static void init(int bufferCapacity, boolean displayDebugInfo)
	{
		_buffer = new IntRingBuffer(bufferCapacity);
		_displayDebugInfo = displayDebugInfo;
	}

	/**
	 * Gets called by IRQ1 Interrupt
	 */
	public static void handleInput()
	{
		int code = MAGIC.rIOs8(0x60) & 0xFF; // Unsigned Byte

		// Toggle lock states
		if (code == 58)
			KeyboardState.capsLockActive = !KeyboardState.capsLockActive;
		if (code == 69)
			KeyboardState.numpadLockActive = !KeyboardState.numpadLockActive;
		if (code == 70)
			KeyboardState.scrollLockActive = !KeyboardState.scrollLockActive;

		_buffer.tryWrite(code);
	}

	/**
	 * Process the inputs saved in handleInput() by interrupts
	 */
	public static void processInput()
	{
		if (_buffer.size() > 0)
		{
			for (int i = 0; i < _buffer.size(); i++)
			{
				int code = _buffer.read();
				if (_displayDebugInfo) Console.print(code);

				// E1 is only used for Pause (i think?) and is 3 bytes long
				// So I only want to act at the 3rd byte
				if (_currentE1Pos >= 0) // One of the last 2 codes was 0xE1
				{
					_currentE1Pos++; // Increment current position
					if (_currentE1Pos == 2) // Only handle input at the 3rd (last) byte, ignore 2nd byte. For Pause-Key: Handling this E1 is done in Scancode = 69
						_currentE1Pos = -1; // Reset pos
					else continue;
				}

				// Handle all possible Codes from keyboard
				if (code >= 0x01 && code <= 0x5D) // Make
				{
					if (_displayDebugInfo) Console.print('M');
					int keyCode = getKeyCode(code); // Convert scanCode to keyCode
					toggleState(keyCode, true); // Toggle keyState, if this key is important
					KeypressHandler.onKeyDown(keyCode); // call Event

					if (KeyboardState.e0Active) // E0 is 2 bytes long. If last byte was e0, deactivate now
						toggleState(0xE0, false);
				}
				else if (code >= 0x81 && code <= 0xDD) // Break
				{
					if (_displayDebugInfo) Console.print('B');
					int keyCode = getKeyCode(code & 127); // Convert scanCode to keyCode. For break, we have to invert highest bit
					toggleState(keyCode, false); // Toggle keyState, if this key is important
					KeypressHandler.onKeyUp(keyCode); // call Event

					if (KeyboardState.e0Active) // E0 is 2 bytes long. If last byte was e0, deactivate now
						toggleState(0xE0, false);
				}
				else if (code == 0xE0) // Extension 0
				{
					if (_displayDebugInfo) Console.print("E0");
					toggleState(0xE0, true); // E0 is 2 bytes long. Will be deactivated by make/break codes
				}
				else if (code == 0xE1) // Extension 1
				{
					if(_displayDebugInfo) Console.print("E1");

					toggleState(0xE1, !KeyboardState.e1Active); // Toggle E1 state.
					if (_currentE1Pos < 0) // E1 is 3 bytes long. Is handled at the beginning of this method
						_currentE1Pos = 0;
				}
				else if (code == 0x00) Console.print('O'); // Overflow
				else if (code == 0xFF) Console.print('E'); // Key Error
				else if (code == 0xFA) Console.print('A'); // Acknowledge
				else if (code == 0xFE) Console.print('R'); // Resend
			}
			if(_displayDebugInfo) Console.print(' ');
		}
	}

	/**
	 * Convert a Scancode to the keyCode of the actually pressed key. Considers currently pressed modifier keys.
	 * @param scanCode
	 * @return
	 */
	private static int getKeyCode(int scanCode)
	{
		int keyCode = -1;
		// Get information that has an impact on the resulting keyCode
		boolean uppercase = false;
		if ((KeyboardState.shiftActive && !KeyboardState.capsLockActive) || !KeyboardState.shiftActive && KeyboardState.capsLockActive) uppercase = true;
		boolean altgr = KeyboardState.altgrActive;
		boolean numlock = KeyboardState.numpadLockActive;
		boolean e0 = KeyboardState.e0Active;
		boolean e1 = KeyboardState.e1Active;

		switch (scanCode)
		{
			case 1:
				keyCode = Keycode.Escape;
				break;
			case 2:
				keyCode = Keycode.n1;
				if (uppercase) keyCode = Keycode.ExclamationMark;
				break;
			case 3:
				keyCode = Keycode.n2;
				if (uppercase) keyCode = Keycode.QuotationMark;
				break;
			case 4:
				keyCode = Keycode.n3;
				if (uppercase) keyCode = Keycode.Paragraph;
				break;
			case 5:
				keyCode = Keycode.n4;
				if (uppercase) keyCode = Keycode.Dollar;
				break;
			case 6:
				keyCode = Keycode.n5;
				if (uppercase) keyCode = Keycode.Percent;
				break;
			case 7:
				keyCode = Keycode.n6;
				if (uppercase) keyCode = Keycode.And;
				break;
			case 8:
				keyCode = Keycode.n7;
				if (uppercase) keyCode = Keycode.Slash;
				if (altgr) keyCode = Keycode.LeftCurlyBracket;
				break;
			case 9:
				keyCode = Keycode.n8;
				if (uppercase) keyCode = Keycode.LeftParanthesis;
				if (altgr) keyCode = Keycode.LeftBracket;
				break;
			case 10:
				keyCode = Keycode.n9;
				if (uppercase) keyCode = Keycode.RightParanthesis;
				if (altgr) keyCode = Keycode.RightBracket;
				break;
			case 11:
				keyCode = Keycode.n0;
				if (uppercase) keyCode = Keycode.Equal;
				if (altgr) keyCode = Keycode.RightCurlyBracket;
				break;
			case 12:
				keyCode = Keycode.Ger_SharpS;
				if (uppercase) keyCode = Keycode.QuestionMark;
				if (altgr) keyCode = Keycode.Backslash;
				break;
			case 13:
				keyCode = Keycode.AcuteAccent;
				if (uppercase) keyCode = Keycode.BackQuote;
				break;
			case 14:
				keyCode = Keycode.Backspace;
				break;
			case 15:
				keyCode = Keycode.Tab;
				break;
			case 16:
				keyCode = Keycode.q;
				if (uppercase) keyCode = Keycode.Q;
				if (altgr) keyCode = Keycode.At;
				break;
			case 17:
				keyCode = Keycode.w;
				if (uppercase) keyCode = Keycode.W;
				break;
			case 18:
				keyCode = Keycode.e;
				if (uppercase) keyCode = Keycode.E;
				break;
			case 19:
				keyCode = Keycode.r;
				if (uppercase) keyCode = Keycode.R;
				break;
			case 20:
				keyCode = Keycode.t;
				if (uppercase) keyCode = Keycode.T;
				break;
			case 21:
				keyCode = Keycode.z;
				if (uppercase) keyCode = Keycode.Z;
				break;
			case 22:
				keyCode = Keycode.u;
				if (uppercase) keyCode = Keycode.U;
				break;
			case 23:
				keyCode = Keycode.i;
				if (uppercase) keyCode = Keycode.I;
				break;
			case 24:
				keyCode = Keycode.o;
				if (uppercase) keyCode = Keycode.O;
				break;
			case 25:
				keyCode = Keycode.p;
				if (uppercase) keyCode = Keycode.P;
				break;
			case 26:
				keyCode = Keycode.Ger_UEsmall;
				if (uppercase) keyCode = Keycode.Ger_UE;
				break;
			case 27:
				keyCode = Keycode.Plus;
				if (uppercase) keyCode = Keycode.Asterisk;
				if (altgr) keyCode = Keycode.Tilde;
				break;
			case 28:
				if (e0) keyCode = Keycode.NUM_Enter;
				else keyCode = Keycode.Enter;
				break;
			case 29:
				if (e0) keyCode = Keycode.RControl;
				else keyCode = Keycode.Control;
				break;
			case 30:
				keyCode = Keycode.a;
				if (uppercase) keyCode = Keycode.A;
				break;
			case 31:
				keyCode = Keycode.s;
				if (uppercase) keyCode = Keycode.S;
				break;
			case 32:
				keyCode = Keycode.d;
				if (uppercase) keyCode = Keycode.D;
				break;
			case 33:
				keyCode = Keycode.f;
				if (uppercase) keyCode = Keycode.F;
				break;
			case 34:
				keyCode = Keycode.g;
				if (uppercase) keyCode = Keycode.G;
				break;
			case 35:
				keyCode = Keycode.h;
				if (uppercase) keyCode = Keycode.H;
				break;
			case 36:
				keyCode = Keycode.j;
				if (uppercase) keyCode = Keycode.J;
				break;
			case 37:
				keyCode = Keycode.k;
				if (uppercase) keyCode = Keycode.K;
				break;
			case 38:
				keyCode = Keycode.l;
				if (uppercase) keyCode = Keycode.L;
				break;
			case 39:
				keyCode = Keycode.Ger_OEsmall;
				if (uppercase) keyCode = Keycode.Ger_OE;
				break;
			case 40:
				keyCode = Keycode.Ger_AEsmall;
				if (uppercase) keyCode = Keycode.Ger_AE;
				break;
			case 41:
				keyCode = Keycode.Caret;
				if (uppercase) keyCode = Keycode.Degree;
				break;
			case 42:
				keyCode = Keycode.Shift;
				break;
			case 43:
				keyCode = Keycode.Hash;
				if (uppercase) keyCode = Keycode.Apostrophe;
				break;
			case 44:
				keyCode = Keycode.y;
				if (uppercase) keyCode = Keycode.Y;
				break;
			case 45:
				keyCode = Keycode.x;
				if (uppercase) keyCode = Keycode.X;
				break;
			case 46:
				keyCode = Keycode.c;
				if (uppercase) keyCode = Keycode.C;
				break;
			case 47:
				keyCode = Keycode.v;
				if (uppercase) keyCode = Keycode.V;
				break;
			case 48:
				keyCode = Keycode.b;
				if (uppercase) keyCode = Keycode.B;
				break;
			case 49:
				keyCode = Keycode.n;
				if (uppercase) keyCode = Keycode.N;
				break;
			case 50:
				keyCode = Keycode.m;
				if (uppercase) keyCode = Keycode.M;
				break;
			case 51:
				keyCode = Keycode.Comma;
				if (uppercase) keyCode = Keycode.Semicolon;
				break;
			case 52:
				keyCode = Keycode.Period;
				if (uppercase) keyCode = Keycode.Colon;
				break;
			case 53:
				keyCode = Keycode.Minus;
				if (uppercase) keyCode = Keycode.Underscore;
				if (e0) // This is the numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_Slash;
				}
				break;
			case 54:
				keyCode = Keycode.RShift;
				break;
			case 55:
				// TODO: Print Key sends no signal?
				if (numlock) keyCode = -1;
				else keyCode = Keycode.NUM_Asterisk;
				break;
			case 56:
				keyCode = Keycode.Alt;
				if (e0) keyCode = Keycode.AltGr;
				break;
			case 57:
				keyCode = Keycode.Space;
				break;
			case 58:
				keyCode = Keycode.CapsLock;
				break;
			case 59:
				keyCode = Keycode.F1;
				break;
			case 60:
				keyCode = Keycode.F2;
				break;
			case 61:
				keyCode = Keycode.F3;
				break;
			case 62:
				keyCode = Keycode.F4;
				break;
			case 63:
				keyCode = Keycode.F5;
				break;
			case 64:
				keyCode = Keycode.F6;
				break;
			case 65:
				keyCode = Keycode.F7;
				break;
			case 66:
				keyCode = Keycode.F8;
				break;
			case 67:
				keyCode = Keycode.F9;
				break;
			case 68:
				keyCode = Keycode.F10;
				break;
			case 69:
				keyCode = Keycode.NumLock;
				if (e1) keyCode = Keycode.Pause;
				break;
			case 70:
				keyCode = Keycode.Roll;
				break;
			case 71:
				if (e0)
					keyCode = Keycode.Pos1;
				else // Numlock input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_7;
				}
				break;
			case 72:
				if (e0) keyCode = Keycode.ArrowUp;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_8;
				}
				break;
			case 73:
				if (e0) keyCode = Keycode.PageUp;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_9;
				}
				break;
			case 74:
				if (numlock) keyCode = -1;
				else keyCode = Keycode.NUM_Minus;
				break;
			case 75:
				if (e0) keyCode = Keycode.ArrowLeft;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_4;
				}
				break;
			case 76:
				if (numlock) keyCode = -1;
				else keyCode = Keycode.NUM_5;
				break;
			case 77:
				if (e0) keyCode = Keycode.ArrowRight;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_6;
				}
				break;
			case 78:
				if (numlock) keyCode = -1;
				else keyCode = Keycode.NUM_Plus;
				break;
			case 79:
				if (e0) keyCode = Keycode.End;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_1;
				}
				break;
			case 80:
				if (e0) keyCode = Keycode.ArrowDown;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_2;
				}
				break;
			case 81:
				if (e0) keyCode = Keycode.PageDown;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_3;
				}
				break;
			case 82:
				if (e0) keyCode = Keycode.Insert;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_0;
				}
				break;
			case 83:
				if (e0) keyCode = Keycode.Delete;
				else // Numpad input
				{
					if (numlock) keyCode = -1;
					else keyCode = Keycode.NUM_Comma;
				}
				break;
			case 86:
				keyCode = Keycode.LessThan;
				if (uppercase) keyCode = Keycode.GreaterThan;
				if (altgr) keyCode = Keycode.VerticalBar;
				break;
			case 87:
				keyCode = Keycode.F11;
				break;
			case 88:
				keyCode = Keycode.F12;
				break;
		}
		return keyCode;
	}


	/**
	 * Change pressed state of special keys. Does not work for CapsLock, ScrollLock, NumLock as they are
	 * processed by the interruptHandler directly
	 * @param keyCode
	 * @param toggle
	 */
	private static void toggleState(int keyCode, boolean toggle)
	{
		if (keyCode == Keycode.Shift ||keyCode == Keycode.RShift) KeyboardState.shiftActive = toggle;
		if (keyCode == Keycode.Control || keyCode == Keycode.RControl) KeyboardState.ctrlActive = toggle;
		if (keyCode == Keycode.Command) KeyboardState.commandActive = toggle;
		if (keyCode == Keycode.Alt) KeyboardState.altActive = toggle;
		if (keyCode == Keycode.AltGr) KeyboardState.altgrActive = toggle;

		if (keyCode == 0xE0) KeyboardState.e0Active = toggle;
		if (keyCode == 0xE1) KeyboardState.e1Active = toggle;
	}

	public static class KeyboardState
	{
		public static boolean capsLockActive = false;
		public static boolean numpadLockActive = false;
		public static boolean scrollLockActive = false;

		public static boolean shiftActive = false;
		public static boolean ctrlActive = false;
		public static boolean commandActive = false;
		public static boolean altActive = false;
		public static boolean altgrActive = false;

		public static boolean e0Active = false;
		public static boolean e1Active = false;
	}
}