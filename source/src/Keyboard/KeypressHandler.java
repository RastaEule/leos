package keyboard;

import collections.KeyboardInputReceiverList;
import common.Console;

/**
 * This class gets events from the Keyboard Driver, when a button has been pressed.
 * It is planned, that every Application using KeyboardInputs can implement this as an abstract class or something
 * and get input events
 */
public class KeypressHandler
{
	private static KeyboardInputReceiverList _inputHandler;

	public static void init()
	{
		_inputHandler = new KeyboardInputReceiverList();
	}

	public static void registerInputHandler(IKeyboardInputReceiver handler)
	{
		_inputHandler.add(handler);
	}

	/** Remove an input handler by name**/
	public static void removeInputHandler(String name)
	{
		for (int i = 0; i < _inputHandler.length(); i++){
			IKeyboardInputReceiver handler = _inputHandler.at(0);
			if (handler.getName().equals(name)) {
				_inputHandler.remove(i);
				return;
			}
		}
	}

	/**
	 * Event that a key has been pressed
	 * @param keyCode which key has been pressed
	 */
	public static void onKeyDown(int keyCode)
	{
		// Printing the key is handled by the KeyboardInputReceivers
		//if (Keycode.isPrintable(keyCode))
		//		Console.print(Keycode.getChar(keyCode));
		for (int i = 0; i < _inputHandler.length(); i++)
		{
			_inputHandler.at(i).onKeyDown(keyCode);
		}
	}

	/**
	 * Event that a key has been lifted from the keyboard
	 * @param keyCode Which key has been lifted
	 */
	public static void onKeyUp(int keyCode)
	{
		// TODO: Returns only small letter?
		//if (Keycode.isPrintable(keyCode))
		//	Console.print(Keycode.getChar(keyCode));
	}
}
