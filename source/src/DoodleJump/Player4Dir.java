package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameConstants;
import DoodleJump.core.GameObject;
import DoodleJump.core.Vector2;
import common.StaticV24;
import keyboard.IKeyboardInputReceiver;
import keyboard.Keycode;
import keyboard.KeypressHandler;

/** This is a player class that can move in 4 directions. Useful for debugging**/
public class Player4Dir extends GameObject implements IKeyboardInputReceiver
{
	public Vector2 velocity = Vector2.zero();
	private float _sidemoveStrength = 20;

	private boolean _inputUp = false;
	private boolean _inputDown = false;
	private boolean _inputLeft = false;
	private boolean _inputRight = false;

	public Player4Dir()
	{
		int width = 15;
		int height = 15;
		byte[] bitmap = new byte[width * height]; // Zugriff ist immer b[row][columnn]
		for (int k = 0; k < width * height; k++)
			bitmap[k] = (byte)160;

		setDrawable(new Drawable(bitmap, width, height));
	}

	@Override
	public void update()
	{
		float newvY = 0;
		float newvX = 0;

		// Handle Inputs
		if (_inputUp){
			newvX = 0;
			newvY = -_sidemoveStrength;
		}
		if (_inputDown){
			newvX = 0;
			newvY = _sidemoveStrength;
		}
		if (_inputLeft){
			newvX = -_sidemoveStrength;
			newvY = 0;
		}
		if (_inputRight){
			newvX = _sidemoveStrength;
			newvY = 0;
		}
		// Reset inputs:
		_inputUp = false;
		_inputDown = false;
		_inputLeft = false;
		_inputRight = false;

		// Apply new velocity values
		velocity = new Vector2(newvX, newvY);

		// Apply velocity to player and cam
		transform.worldPos = transform.worldPos.plus(velocity);
		GameScene.Instance.camera.transform.worldPos.set(new Vector2( transform.worldPos.x - GameConstants.SCREEN_WIDTH / 2,
				transform.worldPos.y - GameConstants.SCREEN_HEIGHT / 2));
	}

	@Override
	public void onKeyDown(int keyCode)
	{
		//StaticV24.println("Player received an input! pos=".add(transform.worldPos.toVector2Int().toString()));
		if (keyCode == Keycode.ArrowUp) {
			_inputUp = true;
		}
		if (keyCode == Keycode.ArrowDown){
			_inputDown = true;
		}
		if (keyCode == Keycode.ArrowLeft) {
			_inputLeft = true;
		}
		if (keyCode == Keycode.ArrowRight){
			_inputRight = true;
		}
	}

	@Override
	public void onKeyUp(int keyCode)
	{

	}

	public String getName()
	{
		return "Player4Dir";
	}
}
