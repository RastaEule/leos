package DoodleJump.core;

import common.Convert;
import common.math;

/**
 * Representation of 2D vectors and points.
 */
public class Vector2
{
	public float x = 0;
	public float y = 0;

	public Vector2()
	{
	}

	public Vector2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2(Vector2 v)
	{
		this.x = v.x;
		this.y = v.y;
	}

	/** Convert this Vector2 into an instance of Vector2Int with rounded values. **/
	public Vector2Int toVector2Int()
	{
		return new Vector2Int((int)this.x, (int)this.y);
	}

	/**
	 * Returns the length of this vector
	 */
	public float magnitude()
	{
		return (float) math.sqrt(x * x + y * y);
	}

	/**
	 * Returns the squared length of this vector
	 */
	public float sqrMagnitude()
	{
		return (float)(x * x + y * y);
	}

	/**
	 * Returns this vector with a magnitude of 1 (Read Only).
	 * When normalized, a vector keeps the same direction but its length is 1.0.
	 * Note that the current vector is unchanged and a new normalized vector is returned. If you want to normalize the current vector, use normalize function.
	 * @return
	 */
	public Vector2 normalized()
	{
		float len = this.magnitude();
		return new Vector2(this.x / len, this.y / len);
	}

	/**
	 * Makes this vector have a magnitude of 1.
	 * When normalized, a vector keeps the same direction but its length is 1.0.
	 * Note that this function will change the current vector. If you want to keep the current vector unchanged, use normalized function.
	 */
	public void normalize()
	{
		this.set(this.normalized());
	}

	/** Clamps the Vector2Int to the bounds given by min and max. **/
	public void clamp(Vector2 min, Vector2 max)
	{
		if (x < min.x)
			x = min.x;
		if (y < min.y)
			y = min.y;

		if (x > max.x)
			x = max.x;
		if (y > max.y)
			y = max.y;
	}

	/** Returns true if the objects are equal. **/
	public boolean equals(Vector2 v)
	{
		return (v.x == this.x && v.y == this.y);
	}

	/** Set x and y components of an existing Vector2. **/
	public void set(Vector2 v)
	{
		this.x = v.x;
		this.y = v.y;
	}

	/** Set x and y components . **/
	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/** Adds two vectors. **/
	public Vector2 plus(Vector2 v)
	{
		return new Vector2(this.x + v.x, this.y + v.y);
	}

	/** Subtracts one vector from another.. **/
	public Vector2 minus(Vector2 v)
	{
		return new Vector2(this.x - v.x, this.y - v.y);
	}

	/** Performs an elementwise multiplication by another vector. **/
	public Vector2 multiply(Vector2 v)
	{
		return new Vector2(this.x * v.x, this.y * v.y);
	}

	/** Multiplies a vector by a number. **/
	public Vector2 multiply(int number)
	{
		return new Vector2(this.x * number, this.y * number);
	}

	/** Multiplies a vector by a number. **/
	public Vector2 multiply(float number)
	{
		return new Vector2(this.x * number, this.y * number);
	}

	/** Returns the distance between this vector and another vector v **/
	public float distance(Vector2 v)
	{
		return Vector2.distance(this, v);
	}

	public String toString()
	{
		return "(".add(Convert.toString(this.x, 10)).add("/").add(Convert.toString(this.y, 10)).add(")");
	}

	/**
	 * Returns the distance between a and b.
	 * Vector2.Distance(a,b) is the same as (a-b).magnitude.
	 */
	public static float distance(Vector2 a, Vector2 b)
	{
		return a.minus(b).magnitude();
	}

	/** Shorthand for writing Vector2Int(0, -1). **/
	public static Vector2 down()
	{
		return new Vector2(0, -1);
	}
	/** Shorthand for writing Vector2Int(-1, 0). **/
	public static Vector2 left()
	{
		return new Vector2(-1, 0);
	}
	/** Shorthand for writing Vector2Int(1, 1). **/
	public static Vector2 one()
	{
		return new Vector2(1, 1);
	}
	/** Shorthand for writing Vector2Int(1, 0). **/
	public static Vector2 right()
	{
		return new Vector2(1, 0);
	}
	/** Shorthand for writing Vector2Int(0, 1). **/
	public static Vector2 up()
	{
		return new Vector2(0, 1);
	}
	/** Shorthand for writing Vector2Int(0, 0). **/
	public static Vector2 zero()
	{
		return new Vector2(0, 0);
	}
}
