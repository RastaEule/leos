package DoodleJump.core;

import DoodleJump.Camera;
import DoodleJump.GameScene;

/**
 * Responsible for rendering all objects
 */
public class GameRenderer
{
	public static byte backgroundColor = (byte)3;
	private static final int BASE = 0xA0000;

	/** Remember pixels that have already beedn rendered*/
	private static boolean[] _rendered = new boolean[GameConstants.SCREEN_WIDTH * GameConstants.SCREEN_HEIGHT];

	/**
	 * Some variables are used as member variables, so theres no need to create new Objects and create unnecessary overhead.
	 */
	private static Vector2Int _localPos;

	public static void render(GameObjectList gameObjects, Camera cam)
	{
		for (int i = 0; i < _rendered.length; i++){
			_rendered[i] = false;
		}

		_localPos = Vector2Int.zero();

		// Draw lower z values first
		// The list scene.gameObjects is already sorted ascending by z
		GameObjectList.GameObjectNode node = gameObjects.nodeAt(0);
		while (node != null) {
			GameObject go = node.data;
			if (go.isDrawable && go.isActive) {
				printObject(go, cam);
			}
			node = node.next;
		}
		clearScreen(GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);
	}

	private static void printObject(GameObject go, Camera cam)
	{
		// Step 1: Convert to local space: is the difference between go and cam
		_localPos.set((int)(go.transform.worldPos.x - cam.transform.worldPos.x),
				(int)(go.transform.worldPos.y - cam.transform.worldPos.y));

		// Step 2: draw pixels
		int index = 0;
		for (int h = 0; h < go.transform.height(); h++){
			for (int w = 0; w < go.transform.width(); w++) {
				int coordinateX = _localPos.x + w;
				int coordinateY = _localPos.y + h;

				int linearCoordinate = coordinateX + coordinateY * GameConstants.SCREEN_WIDTH;
				byte color = go.drawable.bitmap()[index++];
				if (coordinateX >= 0 && coordinateX < GameConstants.SCREEN_WIDTH &&
					coordinateY >= 0 && coordinateY < GameConstants.SCREEN_HEIGHT &&
					!_rendered[linearCoordinate] &&
					color != 0){
					setPixel(coordinateX, coordinateY, color);
					_rendered[linearCoordinate] = true;
				}
			}
		}
	}

	public static void setPixel(int x, int y, int color)
	{
		MAGIC.wMem8(BASE + y * GameConstants.SCREEN_WIDTH + x, (byte)color);
	}

	public static void clearScreen(int width, int height)
	{
		if (backgroundColor == 0)
			return;

		int adr = BASE;
		for (int i = 0; i < width * height; i++)
		{
			// Every Pixel is one byte with a color
			if (!_rendered[i])
				MAGIC.wMem8(adr, backgroundColor);
			adr++;
		}
	}
}
