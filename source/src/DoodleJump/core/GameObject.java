package DoodleJump.core;

import collections.StringList;
import common.Convert;
import common.StaticV24;

/**
 * Base class for all entities.
 * Transform is always added to the GameObject that is being created.
 */
public class GameObject
{
	/** The Transform attached to this GameObject. **/
	public Transform transform = new Transform();

	/** If this GabeObject can be drawn, it has a drawable which contains information on how to draw it.
	 * To change this, use the method updateDrawableTransform().
	 **/
	public Drawable drawable;

	/** Defines whether the GameObject is active in the Scene. **/
	public boolean isActive = true;

	/** Defines whether this GameObject can be drawn or not **/
	public boolean isDrawable = true;

	/** The name of the GameObject **/
	public String name = "GameObject";

	 /** Creates a new game object **/
	public GameObject()
	{
	}

	/** Creates a new game object, named name. **/
	public GameObject(String name)
	{
		this.name = name;
	}

	public void update()
	{
	}

	/**
	 * Update the drawable and set the transform accordingly. So that the width and height match to the representation.
	 * @param d
	 */
	public void setDrawable(Drawable d){
		if (d.width <= 0 ||
		d.height <= 0){
			StaticV24.println("GameObject Error: width/height was not set correctly!");
			return;
		}
		transform.setWidth(d.width);
		transform.setHeight(d.height);
		drawable = d;
		isDrawable = true;
	}

	/**
	 * Removes a GameObject.
	 */
	public void destroy()
	{
		// TODO remove gameObject from scene
	}
}
