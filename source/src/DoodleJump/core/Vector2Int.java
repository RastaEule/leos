package DoodleJump.core;

import common.Convert;
import common.math;

/**
 * Representation of 2D vectors and points using integers.
 * This structure is used in some places to represent 2D positions and vectors that don't require the precision of floating-point.
 */
public class Vector2Int
{
	public int x = 0;
	public int y = 0;

	public Vector2Int()
	{
	}

	public Vector2Int(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2Int(Vector2Int v)
	{
		this.x = v.x;
		this.y = v.y;
	}

	/**
	 * Returns the length of this vector
	 */
	public float magnitude()
	{
		return (float) math.sqrt(x * x + y * y);
	}

	/**
	 * Returns the squared length of this vector
	 */
	public float sqrMagnitude()
	{
		return (float)(x * x + y * y);
	}

	/** Clamps the Vector2Int to the bounds given by min and max. **/
	public void clamp(Vector2Int min, Vector2Int max)
	{
		if (x < min.x)
			x = min.x;
		if (y < min.y)
			y = min.y;

		if (x > max.x)
			x = max.x;
		if (y > max.y)
			y = max.y;
	}

	/** Returns true if the objects are equal. **/
	public boolean equals(Vector2Int v)
	{
		return (v.x == this.x && v.y == this.y);
	}

	/** Set x and y components of an existing Vector2Int. **/
	public void set(Vector2Int v)
	{
		this.x = v.x;
		this.y = v.y;
	}

	/** Set x and y components of an existing Vector2Int. **/
	public void set(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	/** Adds two vectors. **/
	public Vector2Int plus(Vector2Int v)
	{
		return new Vector2Int(this.x + v.x, this.y + v.y);
	}

	/** Subtracts one vector from another.. **/
	public Vector2Int minus(Vector2Int v)
	{
		return new Vector2Int(this.x - v.x, this.y - v.y);
	}

	/** Performs an elementwise multiplication by another vector. **/
	public Vector2Int multiply(Vector2Int v)
	{
		return new Vector2Int(this.x * v.x, this.y * v.y);
	}

	/** Divides a vector by a number. **/
	public Vector2Int multiply(int number)
	{
		return new Vector2Int(this.x * number, this.y * number);
	}

	/** Returns the distance between this vector and another vector v **/
	public float distance(Vector2Int v)
	{
		return Vector2Int.distance(this, v);
	}

	public String toString()
	{
		return "(".add(Convert.toString(this.x, 10)).add("/").add(Convert.toString(this.y, 10)).add(")");
	}

	/**
	 * Returns the distance between a and b.
	 * Vector2.Distance(a,b) is the same as (a-b).magnitude.
	 */
	public static float distance(Vector2Int a, Vector2Int b)
	{
		return a.minus(b).magnitude();
	}

	/** Shorthand for writing Vector2Int(0, -1). **/
	public static Vector2Int down()
	{
		return new Vector2Int(0, -1);
	}
	/** Shorthand for writing Vector2Int(-1, 0). **/
	public static Vector2Int left()
	{
		return new Vector2Int(-1, 0);
	}
	/** Shorthand for writing Vector2Int(1, 1). **/
	public static Vector2Int one()
	{
		return new Vector2Int(1, 1);
	}
	/** Shorthand for writing Vector2Int(1, 0). **/
	public static Vector2Int right()
	{
		return new Vector2Int(1, 0);
	}
	/** Shorthand for writing Vector2Int(0, 1). **/
	public static Vector2Int up()
	{
		return new Vector2Int(0, 1);
	}
	/** Shorthand for writing Vector2Int(0, 0). **/
	public static Vector2Int zero()
	{
		return new Vector2Int(0, 0);
	}
}
