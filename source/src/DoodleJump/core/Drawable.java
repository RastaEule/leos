package DoodleJump.core;

import collections.StringList;
import kernel.InternalClock;

/** Represents an object that can be drawn. Contains information on how to be drawn. **/
public class Drawable
{
	public int width = 0;
	public int height = 0;

	private StringList _representation;
	private byte[][] _bitmap;

	/**
	 * Color from 0-255
	 * https://www.fountainware.com/EXPL/vga_color_palettes.htm
	 **/
	public byte color = 0;

	/**
	 * In which direction is this drawable facing?
	 * true=facing right (original orientation)
	 * false=facing left
	 **/
	private boolean _orientation = true;

	private int _animationPlayCountTotal = 0;
	private int _currentAnimationPlayCount = 0;

	public Drawable(StringList image)
	{
		_representation = image;
	}

	public Drawable(byte[] image, int w, int h)
	{
		_bitmap = new byte[1][w * h];
		_bitmap[0] = image;
		this.width = w;
		this.height = h;
	}

	public Drawable(byte[][] image, int w, int h)
	{
		_bitmap = new byte[image.length][w * h];
		for (int i = 0; i < _bitmap.length; i++)
			_bitmap[i] = image[i];
		this.width = w;
		this.height = h;
	}

	public Drawable(byte[][] image, int w, int h, int animationPlayCount)
	{
		_bitmap = new byte[image.length][w * h];
		for (int i = 0; i < _bitmap.length; i++)
			_bitmap[i] = image[i];
		this.width = w;
		this.height = h;
		_animationPlayCountTotal = animationPlayCount;
	}

	public void flip(boolean orientation)
	{
		if (orientation == _orientation)
			return;
		_orientation = orientation;
		byte[] buffer = new byte[width];

		for (int i = 0; i < _bitmap.length; i++) { // Go through all bitmaps
			for (int h = 0; h < height; h++) {
				for (int w = 0; w < width; w++) {
					buffer[w] = _bitmap[i][h * width + w];
				}
				for (int w = 0; w < width; w++) {
					_bitmap[i][h * width + w] = buffer[width - 1 - w];
				}
			}
		}

	}

	public byte[] bitmap()
	{
		if (_bitmap.length == 1)
			return _bitmap[0];
		else {
			int index = (int) InternalClock.currentTick % _bitmap.length;
			if (index == 0)
				_currentAnimationPlayCount++;
			if (_animationPlayCountTotal > 0 && _currentAnimationPlayCount >= _animationPlayCountTotal)
				index = _bitmap.length - 1;

			return _bitmap[index];
		}
	}
}
