package DoodleJump.core;

import common.Convert;
import common.StaticV24;

/**
 * Contains GameObjects sorted by their z-Value ascending
 */
public class GameObjectList
{
	private GameObjectNode _firstNode, _lastNode;
	private int _length;

	public GameObjectList()
	{
		_length = 0;
	}

	public void add(GameObject data)
	{
		GameObjectNode newNode = new GameObjectNode(data);
		if (_firstNode == null) {
			_firstNode = newNode;
		}
		else {
			if (data.transform.z > _firstNode.data.transform.z) {
				newNode.next = _firstNode;
				newNode.next.prev = newNode;
				_firstNode = newNode;

				_length++;
				return;
			}
			GameObjectNode nn = _firstNode.next;
			while (nn != null) {
				if (data.transform.z > nn.data.transform.z) {
					nn.prev.next = newNode;
					newNode.next = nn;
					newNode.prev = nn.prev;
					nn.prev = newNode;
					_length++;
					return;
				}
				nn = nn.next;
			}
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return;

		GameObjectNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null)
		{
			if (currentNode.next == null)
			{
				_firstNode = null;
				_lastNode = null;
			}
			else
			{
				currentNode.next.prev = null;
				_firstNode = currentNode.next;
			}
		}
		else
		{
			if (currentNode.next == null)
			{
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else
			{
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public GameObject at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		GameObjectNode currentNode = _firstNode;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public GameObjectNode nodeAt(int index)
	{
		GameObjectNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		return currentNode;
	}

	public int length()
	{
		return _length;
	}

	public void debugPrint()
	{
		GameObjectNode n = _firstNode;
		StaticV24.print("GameObjectList len=".add(Convert.toString(_length, 10)));
		while(n != null) {
			StaticV24.print(" | ".add(n._r_type.name.add(" z=".add(Convert.toString(n.data.transform.z, 10)))));
			n = n.next;
		}
		StaticV24.println();
	}

	public class GameObjectNode
	{
		public GameObjectNode next;
		public GameObjectNode prev;
		public GameObject data;

		public GameObjectNode(GameObject data)
		{
			this.data = data;
		}
	}
}
