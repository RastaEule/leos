package DoodleJump.core;

/**
 * Position of an object.
 */
public class Transform
{
	/** Describes the order a GameObjects is drawn. **/
	public int z = 0;

	/** The world space position of the Transform.
	 * This refers to the position of rectMin in world coordinates
	 **/
	public Vector2 worldPos = Vector2.zero();

	private int _width = 0;
	private int _height = 0;

	public Transform()
	{
	}

	public void setWidth(int w){
		_width = w;
	}

	public void setHeight(int h){
		_height = h;
	}

	public int width()
	{
		return _width;
	}

	public int height()
	{
		return _height;
	}
}
