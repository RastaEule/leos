package DoodleJump;

import DoodleJump.core.*;
import common.StaticV24;

/** This is the player of doodle jump. He can jump upwards and move left/right **/
public class Player extends GameObject
{
	public Vector2 velocity = Vector2.zero();
	private float _jumpStrength = 20;
	private float _sidemoveStrength = 20;

	/**
	 * Can the player go from one side to another
	 */
	private boolean _limitXpos = false;
	/**
	 * y Position of last frame
	 */
	private int _lastYPos = 0;

	/**
	 * Highest y position
	 */
	private int _highestY = 0;
	/**
	 * When the game is lost, wait this many frames until showing the lost screen
	 */
	private int _loseWait = 20;
	/**
	 * How long until losing did we wait already
	 */
	private int _loseWaitCounter = 0;
	/**
	 * Is the game lost?
	 */
	private boolean _gameLost = false;

	private static byte[] _bitmap = binimp.ByteData.player;

	public Player()
	{
		setDrawable(new Drawable(_bitmap, 15, 23));
	}

	public void init()
	{
		_lastYPos = (int)transform.worldPos.y;
		_highestY = _lastYPos;
		_loseWaitCounter = 0;
		_gameLost = false;
	}

	@Override
	public void update()
	{
		if (_gameLost) {
			_loseWaitCounter++;
			if (_loseWaitCounter > _loseWait){
				StaticV24.println("Player switches scene");
				isActive = false;
				_gameLost = false;
				GameScene.Instance.switchToScreen(Screens.SCREEN_LOSE);
			}
		}

		float newvY = velocity.y;
		float newvX = velocity.x * 0.5f;
		if (velocity.y <= 0) { // Going up
			newvY *= 0.75f;
			if (velocity.y > -0.2f) { // Start going down if velocity slowed down enough
				newvY = 1;
			}
		}
		else { // Going down
			newvY *= 1.25f;
		}

		// Handle collisions with platforms
		for (int i = 0; i < GameScene.Instance.platforms.length; i++) {
			GameObject plat = GameScene.Instance.platforms[i];
			int playerX = (int) transform.worldPos.x;
			int platLeftxCheck = (int) (plat.transform.worldPos.x - transform.width());
			int platRightxCheck = (int) (plat.transform.worldPos.x + plat.transform.width());
			int newYpos = (int) (transform.worldPos.y + newvY);
			int yDelta = newYpos - _lastYPos;
			if (yDelta >= 0 && _lastYPos + transform.height() <= plat.transform.worldPos.y && newYpos + transform.height() >= plat.transform.worldPos.y && playerX >= platLeftxCheck && playerX <= platRightxCheck) {
				StaticV24.println("Player Jump!");
				GameScene.Instance.inputUp = true;
				GameScene.Instance.inputDown = false;
			}
		}

		// Handle Inputs
		if (!_gameLost) {
			if (GameScene.Instance.inputUp) {
				newvY = -_jumpStrength;
			}
			if (GameScene.Instance.inputDown) {
				newvY = _jumpStrength;
			}
			if (GameScene.Instance.inputLeft) {
				newvX = -_sidemoveStrength;
				drawable.flip(false);
			}
			if (GameScene.Instance.inputRight) {
				newvX = _sidemoveStrength;
				drawable.flip(true);
			}
		}

		// Apply new velocity values
		velocity = new Vector2(newvX, newvY);
		// Apply velocity to player and cam
		transform.worldPos = transform.worldPos.plus(velocity);

		// Limit x Position or switch sides
		if (_limitXpos) {
			if ((int) transform.worldPos.x <= 0)
				transform.worldPos.x = 0;
			if ((int) (transform.worldPos.x + transform.width()) >= GameConstants.SCREEN_WIDTH)
				transform.worldPos.x = GameConstants.SCREEN_WIDTH - transform.width();
		}
		else {
			if ((int) transform.worldPos.x < 0)
				transform.worldPos.x = GameConstants.SCREEN_WIDTH - transform.width();
			if ((int) (transform.worldPos.x + transform.width()) > GameConstants.SCREEN_WIDTH)
				transform.worldPos.x = 0;
		}
		_lastYPos = (int) transform.worldPos.y;
		if (_lastYPos < _highestY)
			_highestY = _lastYPos;

		if (_lastYPos > _highestY + GameConstants.SCREEN_HEIGHT * 1.5f &&
			!_gameLost) {
			_gameLost = true;
			StaticV24.println("Player LOST the Game!");
		}

		if (!_gameLost)
			GameScene.Instance.camera.transform.worldPos.set(new Vector2(0, transform.worldPos.y - GameConstants.SCREEN_HEIGHT / 2));
	}

	/**
	 * Perform a strong jump
	 */
	public void strongJump()
	{
		velocity.y = -_jumpStrength * 2;
	}
}
