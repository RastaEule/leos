package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameConstants;
import DoodleJump.core.GameObject;

public class Bird extends GameObject
{
	private static byte[][] _bitmap = new byte[8][];

	private float _speed = 1f;
	private int _flyDir = -1;

	public Bird(int flyDir, float speed)
	{
		_flyDir = flyDir;
		_speed = speed;

		_bitmap[0] = binimp.ByteData.bird1;
		_bitmap[1] = binimp.ByteData.bird2;
		_bitmap[2] = binimp.ByteData.bird3;
		_bitmap[3] = binimp.ByteData.bird4;
		_bitmap[4] = binimp.ByteData.bird5;
		_bitmap[5] = binimp.ByteData.bird6;
		_bitmap[6] = binimp.ByteData.bird7;
		_bitmap[7] = binimp.ByteData.bird8;

		setDrawable(new Drawable(_bitmap, 16, 16));
	}

	@Override
	public void update()
	{
		transform.worldPos.x = transform.worldPos.x + _flyDir * _speed;

		if (_flyDir < 0 && transform.worldPos.x < GameScene.Instance.camera.transform.worldPos.x - transform.width()){
			transform.worldPos.x = GameScene.Instance.camera.transform.worldPos.x + GameConstants.SCREEN_WIDTH;
		}
		else if (_flyDir > 0 && transform.worldPos.x > GameScene.Instance.camera.transform.worldPos.x + GameConstants.SCREEN_WIDTH){
			transform.worldPos.x = GameScene.Instance.camera.transform.worldPos.x - transform.width();
		}
	}
}
