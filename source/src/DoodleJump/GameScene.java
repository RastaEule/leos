package DoodleJump;

import DoodleJump.core.*;
import common.Convert;
import common.Random;
import common.StaticV24;
import common.math;
import keyboard.IKeyboardInputReceiver;
import keyboard.Keycode;
import keyboard.KeypressHandler;

/**
 * Contains information about the game scene
 */
public class GameScene implements IKeyboardInputReceiver
{
	public static GameScene Instance;

	public Camera camera;
	public Player player;
	public Random random;
	public GameObjectList gameObjects_gameScreen;
	public GameObjectList gameObjects_titleScreen;
	public GameObjectList gameObjects_loseScreen;
	public GameObject[] platforms;
	public int currentScreen = Screens.SCREEN_TITLE;

	private byte _backGroundColor = 0x03;
	/** How many platforms should exist in the pool**/
	private int _platformCount = 5;
	/** How far away should the platforms be between each other max on the y-Axis. **/
	private int _platformYDiffMax = 60;
	/** How far away should the platforms be between each other min on the y-Axis. **/
	private int _platformYDiffMin = 28;
	/** y-Coordinate of the highest existing platform **/
	private int _highestPlatform = 0;

	/** The seed for the random generator **/
	private long _randomSeed = 123456789;

	private int _wallPoolCount = 25;
	private GameObject[] _leftWalls;
	private int _highestLeftWall = 0;
	private GameObject[] _rightWalls;
	private int _highestRightWall = 0;

	public boolean inputUp = false;
	public boolean inputDown = false;
	public boolean inputLeft = false;
	public boolean inputRight = false;
	public boolean inputEnter = false;

	public GameScene()
	{
		Instance = this;

		camera = new Camera(GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);

		StaticV24.println("Initializing Random");
		random = new Random(_randomSeed);

		StaticV24.println("Initializing misc");
		currentScreen = Screens.SCREEN_TITLE;
		KeypressHandler.registerInputHandler(this);

		switchToScreen(Screens.SCREEN_TITLE);
	}

	public void update()
	{
		// Select gameObjects of current scene

		GameObjectList.GameObjectNode node = null;
		if (currentScreen == Screens.SCREEN_GAME)
			node = gameObjects_gameScreen.nodeAt(0);
		else if (currentScreen == Screens.SCREEN_TITLE)
			node = gameObjects_titleScreen.nodeAt(0);
		else if(currentScreen == Screens.SCREEN_LOSE)
			node = gameObjects_loseScreen.nodeAt(0);

		// Update all gameObjects
		while(node != null) {
			GameObject go = node.data;
			if (go != null && go.isActive)
				go.update();
			node = node.next;
		}

		if (currentScreen == Screens.SCREEN_GAME){
			updateGameScreen();
		}
		else if (currentScreen == Screens.SCREEN_LOSE){
			updateLoseScreen();
		}

		//StaticV24.println("Start Rendering");
		if (currentScreen == Screens.SCREEN_GAME)
			GameRenderer.render(gameObjects_gameScreen, camera);
		else if (currentScreen == Screens.SCREEN_TITLE)
			GameRenderer.render(gameObjects_titleScreen, camera);
		else if (currentScreen == Screens.SCREEN_LOSE)
			GameRenderer.render(gameObjects_loseScreen, camera);
		//StaticV24.println("RenderingDone.");

		// Reset inputs:
		inputUp = false;
		inputDown = false;
		inputLeft = false;
		inputRight = false;
		inputEnter = false;
	}

	private void initGameScreen()
	{
		StaticV24.println("==== Initializing Game Screen ====");

		gameObjects_gameScreen = new GameObjectList();
		GameRenderer.backgroundColor = _backGroundColor;
		camera.transform.worldPos.y = 0;

		StaticV24.println("Initializing left walls");
		_leftWalls = new GameObject[_wallPoolCount];
		for (int i = 0; i < _wallPoolCount; i++) {
			_leftWalls[i] = new Wall();
			int xoord = (int)camera.transform.worldPos.x;
			int ycoord = GameConstants.SCREEN_HEIGHT - i * _leftWalls[i].transform.height();
			_highestLeftWall = ycoord;
			_leftWalls[i].transform.worldPos.set(xoord, ycoord);
			_leftWalls[i].transform.z = 10;
			gameObjects_gameScreen.add(_leftWalls[i]);
		}
		StaticV24.println("Initializing right walls");
		_rightWalls = new GameObject[_wallPoolCount];
		for (int i = 0; i < _wallPoolCount; i++) {
			_rightWalls[i] = new Wall();
			int xcoord = GameConstants.SCREEN_WIDTH - _rightWalls[i].transform.width();
			int ycoord = GameConstants.SCREEN_HEIGHT - i * _rightWalls[i].transform.height();
			_highestRightWall = ycoord;
			_rightWalls[i].transform.worldPos.set(xcoord, ycoord);
			_rightWalls[i].transform.z = 10;
			gameObjects_gameScreen.add(_rightWalls[i]);
		}

		StaticV24.println("Initializing Platforms");
		platforms = new GameObject[_platformCount];
		for (int i = 0; i < _platformCount; i++) {
			GameObject plat = new Platform();
			int xcoord = random.nextInt(GameConstants.SCREEN_WIDTH - plat.transform.width());
			int ycoord = GameConstants.SCREEN_HEIGHT - i * plat.transform.height() - i * _platformYDiffMax;
			_highestPlatform = ycoord;
			plat.transform.worldPos.set(xcoord, ycoord);
			plat.transform.z = 5;
			platforms[i] = plat;
			gameObjects_gameScreen.add(plat);
		}

		StaticV24.println("Initializing player");
		if (player == null)
			player = new Player();
		//p = new Player4Dir();
		player.isActive = true;
		player.transform.worldPos = new Vector2(GameConstants.SCREEN_WIDTH / 2, GameConstants.SCREEN_HEIGHT / 2);
		player.transform.z = 9;
		player.init();
		player.strongJump();
		gameObjects_gameScreen.add(player);

		gameObjects_gameScreen.debugPrint();
	}

	private void initTitleScreen()
	{
		StaticV24.println("==== Initializing Title Screen ====");

		gameObjects_titleScreen = new GameObjectList();
		GameRenderer.backgroundColor = 0;
		camera.transform.worldPos.y = 0;

		TitleScreen ts = new TitleScreen();
		ts.transform.z = 0;
		ts.transform.worldPos.set(camera.transform.worldPos);
		gameObjects_titleScreen.add(ts);

		FloatingObject fo = new FloatingObject(binimp.ByteData.leos_logo, 264, 65, 30, 8);
		fo.transform.z = 10;
		fo.transform.worldPos.x = 28;
		fo.init();
		gameObjects_titleScreen.add(fo);

		fo = new FloatingObject(binimp.ByteData.jumpGameTitle, 264, 65, 18, 10);
		fo.transform.z = 10;
		fo.transform.worldPos.set(28, 60);
		fo.init();
		gameObjects_titleScreen.add(fo);

		fo = new FloatingObject(binimp.ByteData.textStartWithEnter, 107, 10, 10, 15);
		fo.transform.z = 10;
		fo.transform.worldPos.set(106, 135);
		fo.init();
		gameObjects_titleScreen.add(fo);

		Bird bird = new Bird(-1, 1.05f);
		bird.transform.z = 12;
		bird.transform.worldPos.set(280, 50);
		bird.drawable.flip(true);
		gameObjects_titleScreen.add(bird);
		bird = new Bird(-1, 1f);
		bird.transform.z = 12;
		bird.transform.worldPos.set(300, 60);
		gameObjects_titleScreen.add(bird);
		bird = new Bird(-1, 0.9f);
		bird.transform.z = 5;
		bird.transform.worldPos.set(285, 45);
		gameObjects_titleScreen.add(bird);
	}

	private void initLoseScreen()
	{
		StaticV24.println("==== Initializing Lose Screen ====");
		gameObjects_loseScreen = new GameObjectList();
		camera.transform.worldPos.y = -GameConstants.SCREEN_HEIGHT;

		FloatingObject fo = new FloatingObject(binimp.ByteData.textLost, 244, 55, 50, 15);
		fo.transform.z = 10;
		fo.transform.worldPos.set((GameConstants.SCREEN_WIDTH - 244) / 2, 60);
		fo.init();
		gameObjects_loseScreen.add(fo);

		fo = new FloatingObject(binimp.ByteData.textReturn, 150, 11, 10, 15);
		fo.transform.z = 10;
		fo.transform.worldPos.set((GameConstants.SCREEN_WIDTH - 150) / 2, 150);
		fo.init();
		gameObjects_loseScreen.add(fo);

		Clouds clouds = new Clouds(1, 1);
		clouds.transform.z = 0;
		gameObjects_loseScreen.add(clouds);
		clouds = new Clouds(1, 1);
		clouds.transform.worldPos.x = clouds.transform.width();
		clouds.transform.z = 0;
		gameObjects_loseScreen.add(clouds);

		Farlands farlands = new Farlands();
		farlands.transform.z = 0;
		farlands.transform.worldPos.y = GameConstants.SCREEN_HEIGHT - farlands.transform.height();
		gameObjects_loseScreen.add(farlands);
	}

	private void updateGameScreen()
	{
		updateWalls();
		updatePlatforms();
	}

	private void updateLoseScreen()
	{
		if (camera.transform.worldPos.y < 0)
			camera.transform.worldPos.y = camera.transform.worldPos.y + 7;
		else{
			camera.transform.worldPos.y = 0;
		}
	}

	private void updateWalls()
	{
		// Left Walls
		for (int i = 0; i < _wallPoolCount; i++) {
			GameObject lw = _leftWalls[i];
			if ((int)lw.transform.worldPos.y > (int)camera.transform.worldPos.y + GameConstants.SCREEN_HEIGHT){
				lw.transform.worldPos.set(lw.transform.worldPos.x, _highestLeftWall + lw.transform.height());
				_highestLeftWall -= lw.transform.height();
			}
		}
		// Right walls
		for (int i = 0; i < _wallPoolCount; i++) {
			GameObject rw = _rightWalls[i];
			if ((int)rw.transform.worldPos.y > (int)camera.transform.worldPos.y + GameConstants.SCREEN_HEIGHT){
				rw.transform.worldPos.set(rw.transform.worldPos.x, _highestRightWall + rw.transform.height());
				_highestRightWall -= rw.transform.height();
			}
		}
	}

	private void updatePlatforms()
	{
		for (int i = 0; i < platforms.length; i++) {
			GameObject plat = platforms[i];
			if ((int)plat.transform.worldPos.y > (int)camera.transform.worldPos.y + GameConstants.SCREEN_HEIGHT){
				int xcoord = random.nextInt(GameConstants.SCREEN_WIDTH - plat.transform.width());

				int ycoordMax = _highestPlatform - plat.transform.height() - _platformYDiffMax;
				int ycoordMin = _highestPlatform - plat.transform.height() - _platformYDiffMin;
				int ycoord = ycoordMin - random.nextInt(math.abs(ycoordMax - ycoordMin));
				StaticV24.println("New platform ypos=".add(Convert.toString(ycoord, 10)));
				plat.transform.worldPos.set(xcoord, ycoord);
				_highestPlatform = ycoord;
			}
		}
	}

	public void switchToScreen(int screen)
	{
		StaticV24.println("Switching to scene ".add(Convert.toString(screen, 10)));
		currentScreen = screen;

		// Initialize screens
		if (currentScreen == Screens.SCREEN_GAME){
			this.initGameScreen();
		}
		else if (currentScreen == Screens.SCREEN_TITLE){
			this.initTitleScreen();
		}
		else if (currentScreen == Screens.SCREEN_LOSE){
			this.initLoseScreen();
		}
	}

	@Override
	public void onKeyDown(int keyCode)
	{
		if (keyCode == Keycode.ArrowUp) {
			inputUp = true;
		}
		if (keyCode == Keycode.ArrowDown){
			inputDown = true;
		}
		if (keyCode == Keycode.ArrowLeft) {
			inputLeft = true;
		}
		if (keyCode == Keycode.ArrowRight){
			inputRight = true;
		}
		if (keyCode == Keycode.Enter || keyCode == Keycode.NUM_Enter){
			inputEnter = true;
			StaticV24.println("Screen ".add(Convert.toString(currentScreen, 10)).add(" received input ENTER"));
			// Select active scene
			if (currentScreen == Screens.SCREEN_TITLE){
				this.switchToScreen(Screens.SCREEN_GAME);
			}
			else if (currentScreen == Screens.SCREEN_GAME) {
				this.switchToScreen(Screens.SCREEN_TITLE);
			}
			else if (currentScreen == Screens.SCREEN_LOSE){
				this.switchToScreen(Screens.SCREEN_TITLE);
			}
		}
	}

	@Override
	public void onKeyUp(int keyCode)
	{
	}

	public String getName()
	{
		return "GameScene";
	}
}