package DoodleJump;


import DoodleJump.core.Drawable;
import DoodleJump.core.GameObject;
import common.Convert;
import common.StaticV24;
import common.math;
import kernel.InternalClock;

/**
 * Objects thats continously moving up and down
 */
public class FloatingObject extends GameObject
{
	/** How far the object should travel**/
	private int _dist = 10;
	/** How long the animation should take **/
	private float _period = 10f;
	private int _startPos = 0;
	private int _targetPos = 0;
	private boolean _dir = true;
	/** Current internal time **/
	private int _time = 0;

	public FloatingObject(byte[] bitmap, int width, int height, float period, int dist)
	{
		_period = period;
		_dist = dist;
		setDrawable(new Drawable(bitmap, width, height));
	}

	/**
	 * Additional initialization after the position of this object has been set.
	 */
	public void init()
	{
		_startPos = (int)transform.worldPos.y;
		_targetPos = _startPos + _dist;
		_time = 0;
	}

	@Override
	public void update()
	{
		if (_time <= _period && _time >= 0){
			transform.worldPos.y = math.lerp(_startPos, _targetPos, math.smoothstep(_time / _period));
		}
		else
			_dir = !_dir; // Reverse movement dir

		// Set movement direction by changing time:
		if (_dir)
			_time++;
		else
			_time--;
	}
}
