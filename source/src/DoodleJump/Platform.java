package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameObject;
import common.Convert;
import common.StaticV24;

public class Platform extends GameObject
{
	private static byte[] _bitmap = binimp.ByteData.platform;

	public Platform()
	{
		setDrawable(new Drawable(_bitmap, 50, 10));
	}
}
