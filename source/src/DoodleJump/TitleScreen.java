package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameObject;
import common.Convert;
import common.StaticV24;

public class TitleScreen extends GameObject
{
	private static byte[] _bitmap = binimp.ByteData.titleScreen;

	public TitleScreen()
	{
		int width = 320;
		int height = 200;
		setDrawable(new Drawable(_bitmap, width, height));
	}
}
