package DoodleJump;

import DoodleJump.core.GameConstants;
import DoodleJump.core.GameObject;
import DoodleJump.core.Vector2;
import DoodleJump.core.Vector2Int;

public class Camera extends GameObject
{
	public Camera(int width, int height)
	{
		transform.setHeight(GameConstants.SCREEN_HEIGHT);
		transform.setWidth(GameConstants.SCREEN_WIDTH);
		isDrawable = false;
	}
}
