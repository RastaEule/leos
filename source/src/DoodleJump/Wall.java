package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameObject;

public class Wall extends GameObject
{
	private static byte[] _bitmap = binimp.ByteData.wall;

	public Wall()
	{
		setDrawable(new Drawable(_bitmap, 10, 10));
	}
}
