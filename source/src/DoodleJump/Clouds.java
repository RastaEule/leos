package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameConstants;
import DoodleJump.core.GameObject;

public class Clouds extends GameObject
{
	private static byte[] _bitmap = binimp.ByteData.clouds;
	private int _dir = 1;
	private float _speed = 1f;

	public Clouds(int dir, float speed)
	{
		_dir = dir;
		_speed = speed;
		setDrawable(new Drawable(_bitmap, 320, 33));
	}

	@Override
	public void update()
	{
		transform.worldPos.x = transform.worldPos.x + _dir * _speed;

		if (_dir < 0 && transform.worldPos.x < GameScene.Instance.camera.transform.worldPos.x - transform.width()){
			transform.worldPos.x = GameScene.Instance.camera.transform.worldPos.x + GameConstants.SCREEN_WIDTH;
		}
		else if (_dir > 0 && transform.worldPos.x > GameScene.Instance.camera.transform.worldPos.x + GameConstants.SCREEN_WIDTH){
			transform.worldPos.x = GameScene.Instance.camera.transform.worldPos.x - transform.width();
		}
	}
}
