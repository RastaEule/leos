package DoodleJump;

import DoodleJump.core.Drawable;
import DoodleJump.core.GameObject;
import collections.StringList;

public class TestObject1 extends GameObject
{
	public TestObject1()
	{
		int width = 5;
		int height = 5;
		byte[] bitmap = new byte[width * height]; // Zugriff ist immer b[row][columnn]
		for (int k = 0; k < width * height; k++)
			bitmap[k] = (byte)50;

		setDrawable(new Drawable(bitmap, width, height));
	}
}
