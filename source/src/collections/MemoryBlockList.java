package collections;

import kernel.memory.MemoryBlock;

public class MemoryBlockList
{
	private MemoryBlockNode _firstNode, _lastNode;
	private int _length;

	public MemoryBlockList()
	{
		_length = 0;
	}

	public void add(MemoryBlock data)
	{
		MemoryBlockNode newNode = new MemoryBlockNode(data);
		if (_firstNode == null)
		{
			_firstNode = newNode;
		}
		else
		{
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return;

		MemoryBlockNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null)
		{
			if (currentNode.next == null)
			{
				_firstNode = null;
				_lastNode = null;
			}
			else
			{
				currentNode.next.prev = null;
				_firstNode = currentNode;
			}
		}
		else
		{
			if (currentNode.next == null)
			{
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else
			{
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public MemoryBlock at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		MemoryBlockNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public int length()
	{
		return _length;
	}

	private class MemoryBlockNode
	{
		public MemoryBlockNode next;
		public MemoryBlockNode prev;
		public MemoryBlock data;

		public MemoryBlockNode(MemoryBlock data)
		{
			this.data = data;
		}
	}
}
