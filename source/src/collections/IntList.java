package collections;

public class IntList
{
	private IntNode _firstNode, _lastNode;
	private int _length;

	public IntList()
	{
		_length = 0;
	}

	public void add(int data)
	{
		IntNode newNode = new IntNode(data);
		if (_firstNode == null)
		{
			_firstNode = newNode;
		}
		else
		{
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return;

		IntNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null)
		{
			if (currentNode.next == null)
			{
				_firstNode = null;
				_lastNode = null;
			}
			else
			{
				currentNode.next.prev = null;
				_firstNode = currentNode;
			}
		}
		else
		{
			if (currentNode.next == null)
			{
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else
			{
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public int at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		IntNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public int length()
	{
		return _length;
	}

	private class IntNode
	{
		public IntNode next;
		public IntNode prev;
		public int data;

		public IntNode(int data)
		{
			this.data = data;
		}
	}
}