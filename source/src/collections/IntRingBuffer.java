package collections;

import common.Bluescreen;

public class IntRingBuffer
{
	private volatile int _readSeq = 0;
	private volatile int _writeSeq = -1;
	private final int _capacity;
	private int[] _data;

	public IntRingBuffer(int capacity)
	{
		_data = new int[capacity];
		_capacity = capacity;
		_readSeq = 0;
		_writeSeq = -1;
	}

	public boolean tryWrite(int element)
	{
		if (!this.isFull())
		{
			int writePos = (_writeSeq + 1) % _capacity;
			_data[writePos] = element;
			_writeSeq++;
			return true;
		}
		return false;
	}

	public int read()
	{
		if (!this.isEmpty())
		{
			int readPos = _readSeq % _capacity;
			_readSeq++;
			return _data[readPos];
		}
		return -1;
	}

	public int capacity()
	{
		return _capacity;
	}

	public int size()
	{
		return (_writeSeq - _readSeq) + 1;
	}

	public boolean isEmpty()
	{
		return _writeSeq < _readSeq;
	}

	public boolean isFull()
	{
		return size() >= _capacity;
	}
}