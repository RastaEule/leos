package collections;

public class ByteRingbuffer
{
	private byte[] _data;
	private int _size = 0;
	private int _nextReadPos = 0;
	private int _nextWritePos = 0;

	public ByteRingbuffer(int size)
	{
		_size = size;
		_data = new byte[size];
	}

	public void write(byte b)
	{
		_data[_nextWritePos] = b;
		_nextWritePos = (_nextWritePos + 1) % _size;
	}

	public byte read()
	{
		int readPos = _nextReadPos;
		_nextReadPos = (_nextReadPos + 1) % _size;
		return _data[readPos];
	}
}
