package collections;
import kernel.scheduler.Task;

public class TaskList
{
	private TaskNode _firstNode, _lastNode;
	private int _length;

	public TaskList()
	{
		_length = 0;
	}

	public void add(Task data)
	{
		TaskNode newNode = new TaskNode(data);
		if (_firstNode == null) {
			_firstNode = newNode;
		}
		else {
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if (_length <= 0 || index < 0 || index >= _length)
			return;

		TaskNode currentNode = _firstNode;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null) {
			if (currentNode.next == null) {
				_firstNode = null;
				_lastNode = null;
			}
			else {
				currentNode.next.prev = null;
				_firstNode = currentNode;
			}
		}
		else {
			if (currentNode.next == null) {
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else {
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public Task at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		TaskNode currentNode = _firstNode;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public int length()
	{
		return _length;
	}

	private class TaskNode
	{
		public TaskNode next;
		public TaskNode prev;
		public Task data;

		public TaskNode(Task data)
		{
			this.data = data;
		}
	}
}