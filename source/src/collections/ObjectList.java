package collections;

public class ObjectList
{
	private ObjectNode _firstNode, _lastNode;
	private int _length;

	public ObjectList()
	{
		_length = 0;
	}

	public void add(Object data)
	{
		ObjectNode newNode = new ObjectNode(data);
		if (_firstNode == null)
		{
			_firstNode = newNode;
		}
		else
		{
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return;

		ObjectNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null)
		{
			if (currentNode.next == null)
			{
				_firstNode = null;
				_lastNode = null;
			}
			else
			{
				currentNode.next.prev = null;
				_firstNode = currentNode;
			}
		}
		else
		{
			if (currentNode.next == null)
			{
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else
			{
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public Object at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		ObjectNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public int length()
	{
		return _length;
	}

	private class ObjectNode
	{
		public ObjectNode next;
		public ObjectNode prev;
		public Object data;

		public ObjectNode(Object data)
		{
			this.data = data;
		}
	}
}
