package collections;

public class IntStack
{

	private StackElement _topElement = null;
	private int _elementCount = 0;


	/**
	 * Inserts an element into the stack.
	 * @param object The element to place on the stack.
	 */
	public void push(int object) {
		_topElement = new StackElement(object);
		++_elementCount;
	}


	/**
	 * Returns the top element of the stack without removing it from there.
	 * @return Top element of the stack.
	 */
	@SJC.Inline
	public int peek() {
		return _topElement.data;
	}


	/**
	 * Takes the top element of the stack and returns it.
	 * @return Top element of the stack.
	 */
	public int pop() {
		if(_elementCount > 0) {
			int temp = _topElement.data;
			_topElement = _topElement.next;

			// This must only be executed if something is in the stack!
			// Otherwise we might end up with a negative count,
			// resulting in the last element being inaccessible forever.
			--_elementCount;
			return temp;
		}
		return -1;
	}


	/**
	 * The current amount of elements on the stack.
	 * @return Amount of stack elements.
	 */
	@SJC.Inline
	public int getSize() {
		return _elementCount;
	}


	private class StackElement {
		StackElement next;
		int data;

		public StackElement(int object)
		{
			data = object;
			next = _topElement;
		}
	}
}
