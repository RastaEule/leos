package collections;

public class StringList
{
	private StringNode _firstNode, _lastNode;
	private int _length;

	public StringList()
	{
		_length = 0;
	}

	public void add(String data)
	{
		StringNode newNode = new StringNode(data);
		if (_firstNode == null)
		{
			_firstNode = newNode;
		}
		else
		{
			_lastNode.next = newNode;
			newNode.prev = _lastNode;
		}
		_lastNode = newNode;
		_length++;
	}

	public void remove(int index)
	{
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return;

		StringNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		if (currentNode.prev == null)
		{
			if (currentNode.next == null)
			{
				_firstNode = null;
				_lastNode = null;
			}
			else
			{
				currentNode.next.prev = null;
				_firstNode = currentNode;
			}
		}
		else
		{
			if (currentNode.next == null)
			{
				currentNode.prev.next = null;
				_lastNode = currentNode;
			}
			else
			{
				currentNode.prev.next = currentNode.next;
				currentNode.next.prev = currentNode.prev;
			}
		}
		currentNode = null;
		_length--;
	}

	public String at(int index)
	{
		// Keine Überprüfung der Länge und indexe
		// Wenn was schiefgeht, wird halt eine exception geworfen
		/*
		if(_length <= 0 ||
				index < 0 ||
				index >= _length)
			return -1;
		 */
		StringNode currentNode = _firstNode;
		for (int i = 0; i < index; i++)
		{
			currentNode = currentNode.next;
		}
		return currentNode.data;
	}

	public int length()
	{
		return _length;
	}

	private class StringNode
	{
		public StringNode next;
		public StringNode prev;
		public String data;

		public StringNode(String data)
		{
			this.data = data;
		}
	}
}