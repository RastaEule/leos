package common;

import kernel.BIOS;
import rte.SClassDesc;
import rte.SMthdBlock;
import rte.SPackage;

public class Bluescreen
{
	public static void displayBluescreen(String errorMsg)
	{
		BIOS.setGraphicMode80x25();

		StaticV24.indent = 0;
		StaticV24.println();
		StaticV24.print("===== BLUESCREEN: ");
		StaticV24.println(errorMsg);

		// Console output
		Console.setColor(Console.Color.Gray, Console.Color.Blue);
		Console.clearScreen();
		Console.println("OOPSIE WOOPSIE!!");
		Console.println("Uwu We made a fucky wucky!! A wittle fucko boingo!");
		Console.print("Error Message: ");
		Console.println(errorMsg);
		Console.println();
		Console.println("Callstack:");

		// Gather register Data
		int ebp=0; // First EBP that leads me into the stack
		MAGIC.inline(0x89, 0x6D);
		MAGIC.inlineOffset(1, ebp); //mov [ebp+xx],ebp

		// First entry
		int oldEBP = MAGIC.rMem32(ebp);
		// First entry is an interrupt handler, so we have to skip oldEBP and all registers from PUSHA
		// PUSHA contains the registers eax ecx edx ebx old-esp ebp esi edi (8 registers)
		// So we skip oldEBP + 8 Registers from PUSHA, which are each Integers, so 4 bytes large
		int oldEIP = MAGIC.rMem32(ebp + 4 * 9);
		while(oldEBP >= 0x70000 && oldEBP <= 0x9FFFF)
		{
			printCallstackEntry(oldEIP, oldEBP);
			int newEBP = MAGIC.rMem32(oldEBP);
			if (newEBP < oldEBP)
				break;
			oldEBP = newEBP;
			oldEIP = MAGIC.rMem32(oldEBP + 4);
		}
		Console.println();
	}

	private static void printCallstackEntry(int EIP, int oldEBP)
	{
		Console.print("| EIP: ");
		Console.printHex(EIP);
		Console.print("\t");
		Console.print(getPackage(SPackage.root.subPacks, EIP));
		//Console.print("\t\t");
		//Console.print("old EBP: ");
		//Console.printHex(oldEBP);
		Console.println();
	}

	private static String getPackage(SPackage p, int eip)
	{
		SClassDesc clss;
		SMthdBlock mthd;

		while(p != null){
			if (p.subPacks != null){
				String result = getPackage(p.subPacks, eip);
				if (!result.equals(""))
					return result;
			}

			clss = p.units;
			while(clss != null) {
				mthd = clss.mthds;
				while(mthd != null){
					int methodAdr = MAGIC.cast2Ref(mthd);
					int methodStart = methodAdr - mthd._r_relocEntries * MAGIC.ptrSize;
					int methodEnd = methodAdr + mthd._r_scalarSize;
					if (eip >=methodStart && eip < methodEnd){
						return p.name.add(".").add(clss.name).add(".").add(mthd.namePar);
					}
					mthd = mthd.nextMthd;
				}
				clss = clss.nextUnit;
			}
			p = p.nextPack;
		}
		return "";
	}
}
