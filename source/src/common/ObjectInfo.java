package common;

import java.lang.*;

import rte.*;
import collections.*;
import common.*;
import keyboard.*;
import kernel.*;
import kernel.memory.*;
import kernel.scheduler.*;
import DoodleJump.*;
import DoodleJump.core.*;

/** Generated by UpdateObjectInfo.py **/
public class ObjectInfo
{
	public static String getName(Object o)
	{
		if (o == null)
			return "null";
		if (o instanceof String)
			return "String";
		if (o instanceof DynamicRuntime)
			return "DynamicRuntime";
		if (o instanceof SArray)
			return "SArray";
		if (o instanceof SClassDesc)
			return "SClassDesc";
		if (o instanceof SIntfDesc)
			return "SIntfDesc";
		if (o instanceof SIntfMap)
			return "SIntfMap";
		if (o instanceof SMthdBlock)
			return "SMthdBlock";
		if (o instanceof SPackage)
			return "SPackage";
		if (o instanceof ByteRingbuffer)
			return "ByteRingbuffer";
		if (o instanceof CharList)
			return "CharList";
		if (o instanceof CharStack)
			return "CharStack";
		if (o instanceof IntList)
			return "IntList";
		if (o instanceof IntRingBuffer)
			return "IntRingBuffer";
		if (o instanceof IntStack)
			return "IntStack";
		if (o instanceof KeyboardInputReceiverList)
			return "KeyboardInputReceiverList";
		if (o instanceof MemoryBlockList)
			return "MemoryBlockList";
		if (o instanceof ObjectList)
			return "ObjectList";
		if (o instanceof StringList)
			return "StringList";
		if (o instanceof TaskList)
			return "TaskList";
		if (o instanceof Bluescreen)
			return "Bluescreen";
		if (o instanceof CommandHandler)
			return "CommandHandler";
		if (o instanceof Console)
			return "Console";
		if (o instanceof Convert)
			return "Convert";
		if (o instanceof math)
			return "math";
		if (o instanceof ObjectInfo)
			return "ObjectInfo";
		if (o instanceof Random)
			return "Random";
		if (o instanceof Shell)
			return "Shell";
		if (o instanceof StaticV24)
			return "StaticV24";
		if (o instanceof Camera)
			return "Camera";
		if (o instanceof GameScene)
			return "GameScene";
		if (o instanceof Platform)
			return "Platform";
		if (o instanceof Player)
			return "Player";
		if (o instanceof Player4Dir)
			return "Player4Dir";
		if (o instanceof TestObject1)
			return "TestObject1";
		if (o instanceof Wall)
			return "Wall";
		if (o instanceof Drawable)
			return "Drawable";
		if (o instanceof GameConstants)
			return "GameConstants";
		if (o instanceof GameObject)
			return "GameObject";
		if (o instanceof GameObjectList)
			return "GameObjectList";
		if (o instanceof GameRenderer)
			return "GameRenderer";
		if (o instanceof Transform)
			return "Transform";
		if (o instanceof Vector2)
			return "Vector2";
		if (o instanceof Vector2Int)
			return "Vector2Int";
		if (o instanceof BIOS)
			return "BIOS";
		if (o instanceof InternalClock)
			return "InternalClock";
		if (o instanceof Interrupts)
			return "Interrupts";
		if (o instanceof Kernel)
			return "Kernel";
		if (o instanceof PCIBus)
			return "PCIBus";
		if (o instanceof TestObjectA)
			return "TestObjectA";
		if (o instanceof EmptyObject)
			return "EmptyObject";
		if (o instanceof GarbageCollection)
			return "GarbageCollection";
		if (o instanceof MemoryBlock)
			return "MemoryBlock";
		if (o instanceof MemoryManagement)
			return "MemoryManagement";
		if (o instanceof MemoryMap)
			return "MemoryMap";
		if (o instanceof MMU)
			return "MMU";
		if (o instanceof Scheduler)
			return "Scheduler";
		if (o instanceof Task)
			return "Task";
		if (o instanceof TaskBluescreenForce)
			return "TaskBluescreenForce";
		if (o instanceof TaskDoodleJump)
			return "TaskDoodleJump";
		if (o instanceof TaskGarbageCollection)
			return "TaskGarbageCollection";
		if (o instanceof TaskInput)
			return "TaskInput";
		if (o instanceof IKeyboardInputReceiver)
			return "IKeyboardInputReceiver";
		if (o instanceof KeyboardDriver)
			return "KeyboardDriver";
		if (o instanceof Keycode)
			return "Keycode";
		if (o instanceof KeypressHandler)
			return "KeypressHandler";
		if (o instanceof Object)
			return "Object";
		return "Unknown";
	}
}
