package common;

import collections.CharList;
import collections.CharStack;

public class Convert
{
	/**
	 * Table for calculating digits, used in Character, Long, and Integer.
	 */
	private static final char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',};

	public static String toString(long num, int radix)
	{
		// Filter invalid radix
		if (radix < 2 || radix > 35)
			radix = 10;

		// For negative numbers, print out the absolute value w/ a leading '-'.
		// Use an array large enough for a binary number.
		char[] buffer = new char[65];
		int i = 65;
		boolean isNeg = false;
		if (num < 0)
		{
			isNeg = true;
			num = - num;

			// When the value is MIN_VALUE, it overflows when made positive
			if (num < 0)
			{
				buffer[--i] = digits[(int) (-(num + radix % radix))];
				num = -(num / radix);
			}
		}
		do
		{
			buffer[--i] = digits[(int) (num % radix)];
			num /= radix;
		}
		while(num > 0);

		if (isNeg)
			buffer[--i] = '-';

		// Build String
		char[] resultChars = new char[65 - i];
		int counter = 0;
		for (int j = i; j < 65; j++)
			resultChars[counter++] = buffer[j];
		return new String(resultChars);
	}

	public static String toString(int num, int radix)
	{
		return toString((long)num, radix);
	}

	public static String toString(boolean b){
		if (b)
			return "true";
		else
			return "false";
	}

	public static String toString(double number, int precision) {
		// First print all pre-separator digits
		CharStack stack = new CharStack();
		while (number > 0) {
			int digit = ((int)number % 10);
			stack.push((char)(digit + 48));
			number /= 10;
			if(number < 1)
			{
				break;
			}
		}

		int stackSize = stack.getSize();
		CharList chars = new CharList();
		for (int i = 0; i < stackSize; i++) {
			char temp = stack.pop();
			chars.add(temp);
		}
		// Separator
		chars.add('.');

		// Remove the pre-separator digit
		number -= (int)number;

		CharList list = new CharList();
		while (number > 0) {
			// Multiply the after-separator value by 10 to get the digit
			int digit = (int)(number * 10);
			list.add((char)(digit + 48));
			// Num * 10 - digit -> 0.14 * 10 = 1.4, digit 1 -> 1.4 - digit = 0.4
			number = (number * 10) - digit;
		}

		// Take everything from the list and omit everything beyond the precision limit
		int digitCount = list.getLength();
		int digitCounter = 0;
		for (int i = 1; i <= digitCount; i++) {
			if(digitCounter++ >= precision) {
				return new String(chars.toArray());
			}
			char temp = list.elementAt(i);
			chars.add(temp);
		}
		return new String(chars.toArray());
	}

	public static String toString(float number, int precision) {
		return toString((double)number, precision);
	}
}
