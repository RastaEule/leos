package common;

public class Console
{
	public static final int VIDMEMSTART = 0xB8000;
	private static final int SCREEN_WIDTH = 80;
	private static final int SCREEN_HEIGHT = 25;

	private static byte _color = 7;
	private static int _vidPos = VIDMEMSTART;
	private static int _posX = 0;
	private static int _posY = 0;

	public static int foregroundColor = Color.Gray;
	public static int backgroundColor = Color.Black;

	public static void setColor(int foreground, int background)
	{
		_color = getColorByte(foreground, background);
		foregroundColor = foreground;
		backgroundColor = background;
	}

	private static byte getColorByte(int foreground, int background)
	{
		byte color = (byte)background;
		byte fgb = (byte)foreground;
		fgb = (byte) (fgb & 15);

		color = (byte) (color & 15);
		color = (byte) (color << 4);
		color = (byte) (color | fgb);
		color = (byte) (color & 119);
		return color;
	}

	public static void setCursor(int newX, int newY)
	{
		//Calculate new care position based on x, y values and screen width and height

		if (newX >= 0 && newX * 2 < SCREEN_WIDTH * 2)
			_posX = newX;
		else
			_posX = 0;

		if (newY >= 0 && newY < SCREEN_HEIGHT)
			_posY = newY;
		else
			_posY = 0;

		updateCursorPosition();
		updateVidPos();
	}

	private static void updateVidPos()
	{
		int newPos = (_posY * (SCREEN_WIDTH * 2)) + (_posX * 2);
		_vidPos = VIDMEMSTART + newPos;
	}

	public static void println()
	{
		setCursor(0, _posY + 1);
		updateVidPos();
	}

	public static void print(char c)
	{
		if (c == '\n')
		{
			println();
		}
		else if (c == '\t')
		{
			printTab();
		}
		else
		{
			MAGIC.wMem8(_vidPos++, (byte)c);
			MAGIC.wMem8(_vidPos++, _color);

			setCursor(_posX + 1, _posY);
		}
	}

	public static void print(String str)
	{
		for (int i = 0; i < str.length(); i++)
			print(str.charAt(i));
	}

	public static void print(long x)
	{
		printLong(x, 10);
	}

	public static void print(int x)
	{
		printLong(x, 10);
	}

	public static void print(byte x)
	{
		// Java has no unsigned byte, but I want to keep it unsigned
		int num = x & 0xFF;
		printLong(num, 10);
	}

	public static void print(short x) { printLong(x, 10);}

	public static void printHex(byte x)
	{
		// Java has no unsigned byte, but I want to keep it unsigned
		int num = x & 0xFF;
		printLong(num, 16);
	}

	public static void printHex(short x)
	{
		printLong(x, 16);
	}

	public static void printHex(int x)
	{
		printLong(x, 16);
	}

	public static void printHex(long x)
	{
		printLong(x, 16);
	}

	public static void println(char c)
	{
		print(c);
		println();
	}

	public static void println(String str)
	{
		print(str);
		println();
	}

	public static void println(long l)
	{
		print(l);
		println();
	}

	public static void println(int i)
	{
		print(i);
		println();
	}

	public static void println(byte l)
	{
		print(l);
		println();
	}

	public static void println(short l)
	{
		print(l);
		println();
	}

	public static void printLong(long num, int radix)
	{
		print(Convert.toString(num, radix));
	}

	public static void print(boolean b)
	{
		if (b) print("true");
		else print ("false");
	}

	public static void printTab()
	{
		int spaceCount;
		for (spaceCount = 0; spaceCount < 4; spaceCount++)
			if ((_posX + spaceCount) % 4 == 0)
				break;
		if (spaceCount == 0)
			spaceCount = 4;

		for (int i = 0; i < spaceCount; i++)
			Console.print(' ');
	}

	public static void directPrint(char c, int posX, int posY, int foregroundColor, int backgroundColor)
	{
		byte color = getColorByte(foregroundColor, backgroundColor);

		int vidPos = VIDMEMSTART + (posY * (SCREEN_WIDTH * 2)) + (posX * 2);

		MAGIC.wMem8(vidPos++, (byte)c);
		MAGIC.wMem8(vidPos++, color);
	}

	public static void backSpace()
	{
		if (_posX > 0) // can only backspace in a single line
		{
			_vidPos-=2;
			MAGIC.wMem8(_vidPos, (byte)0);
			_posX -= 1;
			updateCursorPosition();
		}
	}

	public static void disableCursor() {
		MAGIC.wIOs8(0x3D4, (byte)0x0A);
		MAGIC.wIOs8(0x3D5, (byte)0x20);
	}


	public static void enableCursor() {
		MAGIC.wIOs8(0x3D4, (byte)0x0A);
		MAGIC.wIOs8(0x3D5, ((byte) (MAGIC.rIOs8(0x3D5) & 0xC0)));

		MAGIC.wIOs8(0x3D4, (byte)0x0B);
		MAGIC.wIOs8(0x3D5, (byte) ((MAGIC.rIOs8(0x3D5) & 0xE0) | SCREEN_HEIGHT));
	}

	public static void clearScreen()
	{
		setCursor(0, 0);
		for (int i = 0; i < SCREEN_HEIGHT; i++)
		{
			clearLine();
			_posY++;
		}
		setCursor(0, 0);
	}

	public static void clearLine()
	{
		setCursor(0, _posY);
		for (int i = 0; i < SCREEN_WIDTH; i++)
			print(' ');
		setCursor(0, _posY);
	}

	public static int getCurrentPosX()
	{
		return _posX;
	}

	public static int getCurrentPosY()
	{
		return _posY;
	}

	/**
	 * Updates the blinking caret.
	 */
	private static void updateCursorPosition()
	{
		// See https://wiki.osdev.org/Text_Mode_Cursor
		int pos = _posY * SCREEN_WIDTH + _posX;

		MAGIC.wIOs8(0x3D4, (byte)0x0F);
		MAGIC.wIOs8(0x3D5, (byte) (pos & 0xFF));
		MAGIC.wIOs8(0x3D4, (byte)0x0E);
		MAGIC.wIOs8(0x3D5, (byte) ((pos >> 8) & 0xFF));
	}

	/**
	 * Colors for console output
	 */
	public static class Color
	{
		public static final byte Black = 0x00;
		public static final byte Blue = 0x01;
		public static final byte Green = 0x02;
		public static final byte Turquoise = 0x03;
		public static final byte Red = 0x04;
		public static final byte Purple = 0x05;
		public static final byte Brown = 0x06;
		public static final byte Gray = 0x07;
	}
}