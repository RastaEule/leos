package common;

public final class math
{
	public static final int INT_MAX = 2147483647;

	/**
	 * The most accurate approximation to the mathematical constant <em>e</em>:
	 * <code>2.718281828459045</code>. Used in natural log and exp.
	 *
	 */
	public static final double E = 2.718281828459045;

	/**
	 * The most accurate approximation to the mathematical constant <em>pi</em>:
	 * <code>3.141592653589793</code>. This is the ratio of a circle's diameter
	 * to its circumference.
	 */
	public static final double PI = 3.141592653589793;
	public static final double DOUBLE_PI = PI * 2f;
	public static final double PI_2 = PI / 2f;

	/** Used for sin/cos **/
	private static final double CONST_1 = 4f / PI;
	/** Used for sin/cos **/
	private static final double CONST_2 = 4f / (PI * PI);

	/**
	 * Take the absolute value of the argument.
	 * (Absolute value means make it positive.)
	 * <P>
	 *
	 * Note that the the largest negative value (Integer.MIN_VALUE) cannot
	 * be made positive.  In this case, because of the rules of negation in
	 * a computer, MIN_VALUE is what will be returned.
	 * This is a <em>negative</em> value.  You have been warned.
	 *
	 * @param i the number to take the absolute value of
	 * @return the absolute value
	 */
	public static int abs(int i)
	{
		return (i < 0) ? -i : i;
	}

	/**
	 * Take the absolute value of the argument.
	 * (Absolute value means make it positive.)
	 * <P>
	 *
	 * Note that the the largest negative value (Long.MIN_VALUE) cannot
	 * be made positive.  In this case, because of the rules of negation in
	 * a computer, MIN_VALUE is what will be returned.
	 * This is a <em>negative</em> value.  You have been warned.
	 *
	 * @param l the number to take the absolute value of
	 * @return the absolute value
	 */
	public static long abs(long l)
	{
		return (l < 0) ? -l : l;
	}

	/**
	 * Take the absolute value of the argument.
	 * (Absolute value means make it positive.)
	 * <P>
	 *
	 * This is equivalent, but faster than, calling
	 * <code>Float.intBitsToFloat(0x7fffffff & Float.floatToIntBits(a))</code>.
	 *
	 * @param f the number to take the absolute value of
	 * @return the absolute value
	 */
	public static float abs(float f)
	{
		return (f <= 0) ? 0 - f : f;
	}

	/**
	 * Take the absolute value of the argument.
	 * (Absolute value means make it positive.)
	 *
	 * This is equivalent, but faster than, calling
	 * <code>Double.longBitsToDouble(Double.doubleToLongBits(a)
	 *       &lt;&lt; 1) &gt;&gt;&gt; 1);</code>.
	 *
	 * @param d the number to take the absolute value of
	 * @return the absolute value
	 */
	public static double abs(double d)
	{
		return (d <= 0) ? 0 - d : d;
	}

	/**
	 * Return whichever argument is smaller.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the smaller of the two numbers
	 */
	public static int min(int a, int b)
	{
		return (a < b) ? a : b;
	}

	/**
	 * Return whichever argument is smaller.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the smaller of the two numbers
	 */
	public static long min(long a, long b)
	{
		return (a < b) ? a : b;
	}

	/**
	 * Return whichever argument is smaller. If either argument is NaN, the
	 * result is NaN, and when comparing 0 and -0, -0 is always smaller.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the smaller of the two numbers
	 */
	public static float min(float a, float b)
	{
		// this check for NaN, from JLS 15.21.1, saves a method call
		if (a != a)
			return a;
		// no need to check if b is NaN; < will work correctly
		// recall that -0.0 == 0.0, but [+-]0.0 - [+-]0.0 behaves special
		if (a == 0 && b == 0)
			return -(-a - b);
		return (a < b) ? a : b;
	}

	/**
	 * Return whichever argument is smaller. If either argument is NaN, the
	 * result is NaN, and when comparing 0 and -0, -0 is always smaller.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the smaller of the two numbers
	 */
	public static double min(double a, double b)
	{
		// this check for NaN, from JLS 15.21.1, saves a method call
		if (a != a)
			return a;
		// no need to check if b is NaN; < will work correctly
		// recall that -0.0 == 0.0, but [+-]0.0 - [+-]0.0 behaves special
		if (a == 0 && b == 0)
			return -(-a - b);
		return (a < b) ? a : b;
	}

	/**
	 * Return whichever argument is larger.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the larger of the two numbers
	 */
	public static int max(int a, int b)
	{
		return (a > b) ? a : b;
	}

	/**
	 * Return whichever argument is larger.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the larger of the two numbers
	 */
	public static long max(long a, long b)
	{
		return (a > b) ? a : b;
	}

	/**
	 * Return whichever argument is larger. If either argument is NaN, the
	 * result is NaN, and when comparing 0 and -0, 0 is always larger.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the larger of the two numbers
	 */
	public static float max(float a, float b)
	{
		// this check for NaN, from JLS 15.21.1, saves a method call
		if (a != a)
			return a;
		// no need to check if b is NaN; > will work correctly
		// recall that -0.0 == 0.0, but [+-]0.0 - [+-]0.0 behaves special
		if (a == 0 && b == 0)
			return a - -b;
		return (a > b) ? a : b;
	}

	/**
	 * Return whichever argument is larger. If either argument is NaN, the
	 * result is NaN, and when comparing 0 and -0, 0 is always larger.
	 *
	 * @param a the first number
	 * @param b a second number
	 * @return the larger of the two numbers
	 */
	public static double max(double a, double b)
	{
		// this check for NaN, from JLS 15.21.1, saves a method call
		if (a != a)
			return a;
		// no need to check if b is NaN; > will work correctly
		// recall that -0.0 == 0.0, but [+-]0.0 - [+-]0.0 behaves special
		if (a == 0 && b == 0)
			return a - -b;
		return (a > b) ? a : b;
	}

	/**
	 * Convert from degrees to radians. The formula for this is
	 * radians = degrees * (pi/180); however it is not always exact given the
	 * limitations of floating point numbers.
	 *
	 * @param degrees an angle in degrees
	 * @return the angle in radians
	 * @since 1.2
	 */
	public static double toRadians(double degrees)
	{
		return (degrees * PI) / 180;
	}

	/**
	 * Convert from radians to degrees. The formula for this is
	 * degrees = radians * (180/pi); however it is not always exact given the
	 * limitations of floating point numbers.
	 *
	 * @param rads an angle in radians
	 * @return the angle in degrees
	 * @since 1.2
	 */
	public static double toDegrees(double rads)
	{
		return (rads * 180) / PI;
	}

	/**
	 * <p>
	 * Returns the sign of the argument as follows:
	 * </p>
	 * <ul>
	 * <li>If <code>a</code> is greater than zero, the result is 1.0.</li>
	 * <li>If <code>a</code> is less than zero, the result is -1.0.</li>
	 * <li>If <code>a</code> is <code>NaN</code>, the result is <code>NaN</code>.
	 * <li>If <code>a</code> is positive or negative zero, the result is the
	 * same.</li>
	 * </ul>
	 *
	 * @param a the numeric argument.
	 * @return the sign of the argument.
	 * @since 1.5.
	 */
	public static double signum(double a)
	{
		if (a > 0)
			return 1.0;
		if (a < 0)
			return -1.0;
		return a;
	}

	public static float smoothstep(float t)
	{
		return t * t * (3f - 2f * t);
	}

	public static final double sin(float x) {
		if (x < -PI) {
			x += DOUBLE_PI;
		} else if (x > PI) {
			x -= DOUBLE_PI;
		}

		return (x < 0f) ? (CONST_1 * x + CONST_2 * x * x)
				: (CONST_1 * x - CONST_2 * x * x);
	}

	public static final double cos(float x) {
		if (x < -PI) {
			x += DOUBLE_PI;
		} else if (x > PI) {
			x -= DOUBLE_PI;
		}

		x += PI_2;

		if (x > PI) {
			x -= DOUBLE_PI;
		}

		return (x < 0f) ? (CONST_1 * x + CONST_2 * x * x)
				: (CONST_1 * x - CONST_2 * x * x);
	}

	public static float lerp(float startVal, float endVal, float percentage)
	{
		return (startVal + (endVal - startVal) * percentage);
	}

	/**
	 * <p>
	 * Returns the sign of the argument as follows:
	 * </p>
	 * <ul>
	 * <li>If <code>a</code> is greater than zero, the result is 1.0f.</li>
	 * <li>If <code>a</code> is less than zero, the result is -1.0f.</li>
	 * <li>If <code>a</code> is <code>NaN</code>, the result is <code>NaN</code>.
	 * <li>If <code>a</code> is positive or negative zero, the result is the
	 * same.</li>
	 * </ul>
	 *
	 * @param a the numeric argument.
	 * @return the sign of the argument.
	 * @since 1.5.
	 */
	public static float signum(float a)
	{
		if (a > 0)
			return 1.0f;
		if (a < 0)
			return -1.0f;
		return a;
	}

	public static double sqrt(double x) {
		if (x == 0) return (double)0;
		double last = 0.0;
		double res = 1.0;
		while (res != last) {
			last = res;
			res = (res + x / res) / 2;
		}
		return res;
	}
}
