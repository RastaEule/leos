package common;
// Taken and modified from https://github.com/jatovm/classpath/blob/0116515b19e73f3589092b47dbfe6acbccd811d4/java/util/Random.java#L76

/**
 * This class generates pseudorandom numbers.  It uses the same
 * algorithm as the original JDK-class, so that your programs behave
 * exactly the same way, if started with the same seed.
 *
 * The algorithm is described in <em>The Art of Computer Programming,
 * Volume 2</em> by Donald Knuth in Section 3.2.1.  It is a 48-bit seed,
 * linear congruential formula.
 *
 * If two instances of this class are created with the same seed and
 * the same calls to these classes are made, they behave exactly the
 * same way.  This should be even true for foreign implementations
 * (like this), so every port must use the same algorithm as described
 * here.
 *
 * If you want to implement your own pseudorandom algorithm, you
 * should extend this class and overload the <code>next()</code> and
 * <code>setSeed(long)</code> method.  In that case the above
 * paragraph doesn't apply to you.
 *
 * This class shouldn't be used for security sensitive purposes (like
 * generating passwords or encryption keys.  See <code>SecureRandom</code>
 * in package <code>java.security</code> for this purpose.
 *
 * For simple random doubles between 0.0 and 1.0, you may consider using
 * Math.random instead.
 *
 * @author Jochen Hoenicke
 * @author Eric Blake (ebb9@email.byu.edu)
 * @status updated to 1.4
 */
public class Random
{
	/**
	 * True if the next nextGaussian is available.  This is used by
	 * nextGaussian, which generates two gaussian numbers by one call,
	 * and returns the second on the second call.
	 *
	 * @serial whether nextNextGaussian is available
	 * @see #nextNextGaussian
	 */
	private boolean haveNextNextGaussian;

	/**
	 * The next nextGaussian, when available.  This is used by nextGaussian,
	 * which generates two gaussian numbers by one call, and returns the
	 * second on the second call.
	 *
	 * @serial the second gaussian of a pair
	 * @see #haveNextNextGaussian
	 */
	private double nextNextGaussian;

	/**
	 * The seed.  This is the number set by setSeed and which is used
	 * in next.
	 *
	 * @serial the internal state of this generator
	 */
	private long seed;

	/**
	 * Creates a new pseudorandom number generator, starting with the
	 * specified seed, using <code>setSeed(seed);</code>.
	 *
	 * @param seed the initial seed
	 */
	public Random(long seed)
	{
		setSeed(seed);
	}

	public void setSeed(long seed)
	{
		this.seed = (seed ^ 0x5DEECE66DL) & ((1L << 48) - 1);
		haveNextNextGaussian = false;
	}

	 /**
	 * @param bits the number of random bits to generate, in the range 1..32
	 * @return the next pseudorandom value
	 */
	protected int next(int bits)
	{
		seed = (seed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
		return (int) (seed >>> (48 - bits));
	}

	/**
	 * This algorithm would return every value with exactly the same
	 * probability, if the next()-method would be a perfect random number
	 * generator.
	 *
	 * The loop at the bottom only accepts a value, if the random
	 * number was between 0 and the highest number less then 1<<31,
	 * which is divisible by upperBound.  The probability for this is high for small
	 * upperBound, and the worst case is 1/2 (for upperBound=(1<<30)+1).
	 *
	 * The special treatment for upperBound = power of 2, selects the high bits of
	 * the random number (the loop at the bottom would select the low order
	 * bits).  This is done, because the low order bits of linear congruential
	 * number generators (like the one used in this class) are known to be
	 * ``less random'' than the high order bits.
	 * @param upperBound the upper bound
	 * @return the next pseudorandom value
	 */
	public int nextInt(int upperBound)
	{
		if (upperBound <= 0)
			StaticV24.println("ERROR in Random: upperBound must be positive");
		if ((upperBound & -upperBound) == upperBound) // i.e., upperBound is a power of 2
			return (int) ((upperBound * (long) next(31)) >> 31);
		int bits, val;
		do
		{
			bits = next(31);
			val = bits % upperBound;
		}
		while (bits - val + (upperBound - 1) < 0);
		return val;
	}

	/**
	 * Generates the next pseudorandom boolean.  True and false have
	 * the same probability.
	 * @return the next pseudorandom boolean
	 */
	public boolean nextBoolean()
	{
		return next(1) != 0;
	}

	/**
	 * Generates the next pseudorandom float uniformly distributed
	 * between 0.0f (inclusive) and 1.0f (exclusive).
	 * @return the next pseudorandom float
	 */
	public float nextFloat()
	{
		return next(24) / (float) (1 << 24);
	}

	/**
	 * Generates the next pseudorandom double uniformly distributed
	 * between 0.0 (inclusive) and 1.0 (exclusive).
	 * @return the next pseudorandom double
	 */
	public double nextDouble()
	{
		return (((long) next(26) << 27) + next(27)) / (double) (1L << 53);
	}
}
