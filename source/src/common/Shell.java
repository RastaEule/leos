package common;
import keyboard.*;

public class Shell implements IKeyboardInputReceiver
{
	private static final int BUFFER_SIZE = 100;

	private char[] _buffer;
	private int _bufferPos = 0;
	private String _lastCommand = "";

	public Shell()
	{
		this.resetBuffer();
		//Console.enableCursor();
	}

	private void executeCommand()
	{
		String input = this.getBufferString();
		this.resetBuffer();
		CommandHandler.processCommand(input);
		Console.println();
	}

	private void addKeyToBuffer(int keyCode)
	{
		if (Keycode.isPrintable(keyCode))
			_buffer[_bufferPos++] = Keycode.getChar(keyCode);
	}

	private void addKeyToBuffer(char c)
	{
		_buffer[_bufferPos++] = c;
	}

	private void resetBuffer()
	{
		if (_buffer == null)
			_buffer = new char[BUFFER_SIZE];
		for (int i = 0; i < BUFFER_SIZE; i++)
			_buffer[i] = ' ';
		_bufferPos = 0;
	}

	private String getBufferString()
	{
		int stringLen = Console.getCurrentPosX();

		char[] chars = new char[stringLen];
		for (int i = 0; i < stringLen; i++)
			chars[i] = _buffer[i];

		return new String(chars);
	}

	@Override
	public void onKeyDown(int keyCode)
	{
		if (keyCode == Keycode.Enter)
		{
			// Save current buffer
			char[] lastBuffer = new char[_bufferPos];
			for (int i = 0; i < _bufferPos; i++) // Deep copy
				lastBuffer[i] = _buffer[i];
			_lastCommand = new String(lastBuffer);

			this.executeCommand();
		}
		else if (keyCode == Keycode.Backspace)
		{
			Console.backSpace();
			_bufferPos--;
			if (_bufferPos < 0)
				_bufferPos = 0;
			_buffer[_bufferPos] = 0;
		}
		else if (keyCode == Keycode.ArrowUp)
		{
			resetBuffer();
			Console.print(_lastCommand);
			// Put lastCommand in current buffer
			for (int i = 0; i < _lastCommand.length(); i++)
				_buffer[i] = _lastCommand.charAt(i);
			_bufferPos = _lastCommand.length();
		}
		else if (keyCode == Keycode.Tab)
		{
			resetBuffer();
			Console.printTab();
		}
		else
		{
			if(Keycode.isPrintable(keyCode))
			{
				this.addKeyToBuffer(keyCode);
				Console.print(Keycode.getChar(keyCode));
			}
		}
	}

	@Override
	public void onKeyUp(int keyCode)
	{

	}

	@Override
	public String getName()
	{
		return "Shell";
	}
}
