package common;

import kernel.BIOS;
import kernel.Kernel;
import kernel.PCIBus;
import kernel.memory.MemoryManagement;
import kernel.scheduler.Scheduler;
import kernel.scheduler.TaskDoodleJump;
import keyboard.KeypressHandler;

public class CommandHandler {
	public static void processCommand(String input) {
		boolean commandRecognized = false;
		Console.println();
		if (input.equals("help")) {
			commandRecognized = true;
			Console.println("Possible Commands:");
			Console.println("  Hello - World!");
			Console.println("  clear - Clear the console");
			Console.println("  testGraphicMode - Display an example of the extended graphicsMode");
			Console.println("  smap - Display memory map");
			Console.println("  sobj - Show all EmptyObjects");
			Console.println("  pciscan - Display all connected devices on the pci-bus");
			Console.println("  game - Launch Doodle Jump");
			Console.print("  enterBluescreen - Throw a breakpoint exception and display bluescreen");
		}
		if (input.equals("Hello")) {
			commandRecognized = true;
			Console.print("World!");
		}
		else if (input.equals("clear")) {
			commandRecognized = true;
			Console.clearScreen();
		}
		else if (input.equals("testGraphicMode")) {
			commandRecognized = true;
			Kernel.testGraphicMode();
		}
		else if (input.equals("smap"))
		{
			commandRecognized = true;
			MemoryManagement.showMemoryMap();
		}
		else if (input.equals("sobj"))
		{
			commandRecognized = true;
			MemoryManagement.showEmptyObjects();
		}
		else if (input.equals("pciscan")) {
			commandRecognized = true;
			PCIBus.performPCIBusScan();
		}
		else if (input.equals("game")) {
			commandRecognized = true;
			StaticV24.println("=============================================================");
			StaticV24.println("Booting DoodleJump");
			KeypressHandler.removeInputHandler("Shell");
			BIOS.setGraphicMode320x200();
			Scheduler.addTask(new TaskDoodleJump(), 0, "DoodleJump");
		}
		else if (input.equals("enterBluescreen")) {
			commandRecognized = true;
			Bluescreen.displayBluescreen("UserToggle");
		}

		if (!commandRecognized) {
			Console.print("Unkown Command");
		}
	}
}
